import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { Accounts } from 'meteor/accounts-base';
import { check } from 'meteor/check';
import { Email } from 'meteor/email';
import { Users } from './users.js';

//    Meteor._sleepForMs(1000);
Meteor.methods({
  'get.last.user'() {
    if (!Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    return Users.findOne()._id;
  },

  'get.users.by.page'(page, perPage) {
    check(page, Number);
    check(perPage, Number);

    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');

    const total = Users.find({ 'profile.rol': { $ne: 'cliente' } }).count();
    const skip = (perPage * page) - perPage;
    let totalPages = 0;

    if (Number.isInteger(total / perPage)) totalPages = total / perPage;
    else totalPages = Math.trunc((total / perPage)) + 1;

    const result = {};

    result.page = page;
    result.perPage = perPage;
    result.total = total;
    result.totalPages = totalPages;
    result.data = Users.find({ 'profile.rol': { $ne: 'cliente' } }, { skip, limit: perPage }).fetch();

    return result;
  },

  'get.clientes.by.page'(page, perPage) {
    check(page, Number);
    check(perPage, Number);

    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');

    const total = Users.find({ 'profile.rol': 'Cliente' }).count();
    const skip = (perPage * page) - perPage;
    let totalPages = 0;

    if (Number.isInteger(total / perPage)) totalPages = total / perPage;
    else totalPages = Math.trunc((total / perPage)) + 1;

    const result = {};

    result.page = page;
    result.perPage = perPage;
    result.total = total;
    result.totalPages = totalPages;
    result.data = Users.find({ 'profile.rol': 'Cliente' }, { skip, limit: perPage }).fetch();

    return result;
  },

  'insert.user'(user) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');

    console.log(user);

    check(user, {
      nombre: String,
      apellidos: String,
      email: String,
      telefono: String,
      mobile: String,
      rol: String,
      direccion: Object,
    });

    const userId = Accounts.createUser({
      email: user.email,
      password: Random.id(),
      profile: user,
      createdAt: new Date(),
    });

    Accounts.sendEnrollmentEmail(userId, user.email);

    return userId;
  },

  'insert.client'(data) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');

    check(data, {
      nombre: String,
      apellidos: String,
      email: String,
      telefono: String,
      mobile: String,
      rol: String,
      direccion: Object,
    });

    return Accounts.createUser({
      email: data.email,
      password: Random.id(),
      profile: data,
      createdAt: new Date(),
    });
  },

  'update.user.info'(id, profile) {
    if (!Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    check(id, String);
    check(profile, Object);

    Users.update(id, { $set: { profile } });
  },
  'update.client.info'(id, profile) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(id, String);
    check(profile, Object);

    Users.update(id, { $set: { profile } });
  },

  'update.username'(id, username) {
    if (!Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    check(id, String);
    check(username, username);

    const user = Users.findOne({ username });
    if (user) {
      throw new Meteor.Error('already-exists', 'Username already exists');
    } else {
      Accounts.setUsername(id, username);
    }
  },

  'email.verification'(id, email) {
    Accounts.sendVerificationEmail(id, [email]);
  },
});
