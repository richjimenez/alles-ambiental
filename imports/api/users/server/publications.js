import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Users } from '../users.js';

// Admin
Meteor.publish('get.users', () => {
  if (Meteor.userId()) return Users.find();
  return [];
});

Meteor.publish('get.user', (id) => {
  check(id, String);
  if (Meteor.userId()) {
    if (id === 'all') {
      return Users.find({}, { limit: 1, fields: { services: 0, emails: 0 } });
    }
    return Users.find({ _id: id });
  }
  return [];
});

Meteor.publish('get.clients', () => Users.find({ 'profile.rol': 'Cliente' }, { fields: { services: 0, emails: 0 } }));
