import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { Products } from './products';

Meteor.publish('get.products', () => {
  if (Meteor.userId()) return Products.find();
  return [];
});
