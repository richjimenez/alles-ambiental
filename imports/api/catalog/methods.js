import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { check } from 'meteor/check';
import { Email } from 'meteor/email';
import { Products } from './products';

// Meteor._sleepForMs(1000);
Meteor.methods({
  'get.products.by.page'(page, perPage) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    const total = Products.find().count();
    const skip = (perPage * page) - perPage;
    let totalPages = 0;

    if (Number.isInteger(total / perPage)) {
      totalPages = total / perPage;
    } else {
      totalPages = Math.trunc((total / perPage)) + 1;
    }

    const result = {};

    result.page = page;
    result.perPage = perPage;
    result.total = total;
    result.totalPages = totalPages;
    result.data = Products.find({}, { skip, limit: perPage }).fetch();

    return result;
  },
  'get.product'(id) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(id, String);
    if (id === 'all') return Products.findOne();
    return Products.findOne({ _id: id });
  },
  'insert.product'(data) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(data, Object);
    const product = data;

    product.created_at = new Date();
    product.created_by = {
      id: Meteor.userId(),
      name: `${Meteor.user().profile.first_name} ${Meteor.user().profile.last_name}`,
    };

    return Products.insert(product);
  },
  'update.product'(data) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(data, Object);
    if (data.tipo === 'Panel') {
      Products.update({ _id: data.id }, {
        $set: {
          nombre: data.nombre,
          watts: data.watts,
          volts: data.volts,
        },
      });
    } else {
      Products.update({ _id: data.id }, {
        $set: {
          nombre: data.nombre,
        },
      });
    }
  },
  'remove.product'(id) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(id, String);
    Products.remove({ _id: id });
  },
});
