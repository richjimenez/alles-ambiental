const Counters = new Mongo.Collection('counters');

// init autoincrement function
const getNextSequence = (name) => {
  Counters.update({ _id: name }, { $inc: { seq: 1 } });
  const ret = Counters.findOne({ _id: name });
  return ret.seq;
};

export { Counters, getNextSequence };
