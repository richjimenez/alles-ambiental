import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Zones from './zones';

Meteor.publish('get.zone', (id) => {
  check(id, String);
  if (Meteor.userId()) {
    if (id === 'all') return Zones.find({}, { limit: 1 });
    return Zones.find({ _id: id });
  }
  return [];
});


Meteor.publish('get.zones', () => {
  if (Meteor.userId()) return Zones.find();
  return [];
});
