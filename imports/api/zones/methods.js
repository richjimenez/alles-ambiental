import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Zones from './zones';

Meteor.methods({
  'get.zones.by.page'(page, perPage) {
    check(page, Number);
    check(perPage, Number);

    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');

    const total = Zones.find().count();
    const skip = (perPage * page) - perPage;
    let tp = 0;

    if (Number.isInteger(total / perPage)) tp = total / perPage;
    else tp = Math.trunc((total / perPage)) + 1;

    const result = {};

    result.page = page;
    result.perPage = perPage;
    result.total = total;
    result.tp = tp;
    result.data = Zones.find({}, { skip, limit: perPage }).fetch();

    return result;
  },
  'insert.zone'(data) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(data, Object);
    return Zones.insert(data);
  },
  'update.zone'(id, data) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(id, String);
    check(data, Object);
    Zones.update({ _id: id }, { $set: data });
  },
  'update.encargados'(id, users) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(id, String);
    check(users, Array);
    // save users on zone
    Zones.update({ _id: id }, { $unset: { encargados: '' } });
    Zones.update({ _id: id }, { $push: { encargados: { $each: users } } });
  },
  'update.vendedores'(id, users) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(id, String);
    check(users, Array);
    // save users on zone
    Zones.update({ _id: id }, { $unset: { vendedores: '' } });
    Zones.update({ _id: id }, { $push: { vendedores: { $each: users } } });
  },
});
