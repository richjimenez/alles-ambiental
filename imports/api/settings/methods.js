import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Settings } from './settings';

Meteor.methods({
  'update.tarifas'(data) {
    check(data, Object);
    Settings.update({ _id: 'tarifas' }, {
      $set: {
        dac: data.dac,
        pbdt: data.pbdt,
        gdmto: data.gdmto,
        gdmth: data.gdmth,
        watt: data.watt,
        cambio: data.cambio,
      },
    });
  },
});
