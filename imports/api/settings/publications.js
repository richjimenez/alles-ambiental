import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { Settings } from './settings';

Meteor.publish('get.settings', () => {
  if (Meteor.userId()) {
    return Settings.find();
  }
  return [];
});
