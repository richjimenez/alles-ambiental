import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { Email } from 'meteor/email';
import { check } from 'meteor/check';
import { Projects } from './projects';
import { Users } from '../users/users';
import { getNextSequence } from '/imports/api/counters/counters';

// Meteor._sleepForMs(1000);
Meteor.methods({
  'get.projects.by.page'(page, perPage) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(page, Number);
    check(perPage, Number);
    const total = Projects.find().count();
    const skip = (perPage * page) - perPage;
    let totalPages = 0;

    if (Number.isInteger(total / perPage)) {
      totalPages = total / perPage;
    } else {
      totalPages = Math.trunc((total / perPage)) + 1;
    }

    const result = {};

    result.page = page;
    result.perPage = perPage;
    result.total = total;
    result.totalPages = totalPages;
    result.data = Projects.find({}, { skip, limit: perPage }).fetch();

    return result;
  },
  'search.projects'(search) {
    check(search, String);
    const query = {
      $and: [{
        $or: [
          { nombre: { $regex: search, $options: '$i' } },
          { folio: Number(search) },
        ],
      }],
    };
    return Projects.find(query, { limit: 50 }).fetch();
  },
  'get.project'(id) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(id, String);
    if (id === 'all') return Projects.findOne();
    return Projects.findOne({ _id: id });
  },
  'insert.project'(data) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(data, Object);
    const project = data;
    const user = Users.findOne({ _id: project.clienteId });
    const client = {
      id: user._id,
      nombre: user.profile.nombre,
      apellidos: user.profile.apellidos,
      email: user.emails[0].address,
      telefono: user.profile.telefono,
      mobile: user.profile.mobile,
      direccion: user.profile.direccion,
    };
    delete project.clienteId;
    project.client = client;
    project.folio = getNextSequence('folio');
    project.createdAt = new Date();
    return Projects.insert(project);
  },
  'remove.project'(id) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(id, String);
    Projects.remove({ _id: id });
  },
  'add.project.consumo'(id, consumo) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(id, String);
    check(consumo, Object);
    Projects.update({ _id: id }, { $push: { consumos: consumo } });
  },
  'update.consumo'(data) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(data, {
      id: String,
      tarifa_cfe: Object,
      voltaje: Number,
      frecuencia: String,
      numero_hilos: String,
      numero_servicio: String,
      zona: String,
      watt: Number,
      cambio: Number,
      consumos: Array,
      generacion: Number,
    });
    Projects.update({ _id: data.id }, {
      $set: {
        tarifa_cfe: data.tarifa_cfe,
        voltaje: data.voltaje,
        frecuencia: data.frecuencia,
        numero_hilos: data.numero_hilos,
        numero_servicio: data.numero_servicio,
        zona: data.zona,
        watt: data.watt,
        cambio: data.cambio,
        consumos: data.consumos,
        generacion: data.generacion,
      },
    });
  },
  'update.project'(data) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(data, Object);
    const project = data;
    const user = Users.findOne({ _id: project.clienteId });
    const client = {
      id: user._id,
      nombre: user.profile.nombre,
      apellidos: user.profile.apellidos,
      email: user.emails[0].address,
      telefono: user.profile.telefono,
      mobile: user.profile.mobile,
      direccion: user.profile.direccion,
    };
    delete project.clienteId;
    project.client = client;
    Projects.update({ _id: project.id }, {
      $set: {
        client: project.client,
        fuente_contacto: project.fuente_contacto,
        estatus: project.estatus,
        notas: project.notas,
        nombre: project.nombre,
        empresa: project.empresa,
      },
    });
  },
  'insert.panel'(id, data) {
    check(id, String);
    check(data, Object);
    Projects.update({ _id: id }, {
      $set: { panel: data },
    });
  },
  'insert.inversores'(id, data) {
    check(id, String);
    check(data, Array);
    Projects.update({ _id: id }, {
      $set: { inversores: data },
    });
  },
  'remove.inversores'(id) {
    check(id, String);
    Projects.update({ _id: id }, {
      $unset: { inversores: '' },
    });
  },
  'insert.contrato'(id, contrato) {
    if (!Meteor.userId()) throw new Meteor.Error('not-authorized');
    check(id, String);
    check(contrato, String);
    Projects.update({ _id: id }, {
      $set: { contrato },
    });
  },
  'enviar.pdf'(data, email) {
    check(data, String);
    check(email, String);
    Email.send({
      to: email,
      from: 'no-reply@alles.com',
      subject: 'Cotización Alles Solar',
      text: 'Adjunta en este correo esta la cotización en PDF',
      attachments: [
        {
          filename: 'cotizacion.pdf',
          content: data,
          encoding: 'base64',
        },
      ],
    });
  },
  'update.payment'(id, paymentType, data) {
    check(id, String);
    check(paymentType, String);
    check(data, Object);
    console.log(Object.keys(data).length);
    if (Object.keys(data).length >= 1) {
      Projects.update({ _id: id }, {
        $set: { pago: paymentType, amortization: data },
      });
    } else {
      Projects.update({ _id: id }, {
        $set: { pago: paymentType },
        $unset: { amortization: '' },
      });
    }
    Projects.update({ _id: id }, {
      $set: { pago: paymentType },
    });
    // Projects.update({ _id: id }, {
    //   $set: { amortization: data },
    // });
  },
});
