import { check } from 'meteor/check';
import { Meteor } from 'meteor/meteor';
import { Projects } from './projects';

Meteor.publish('get.project', (id) => {
  check(id, String);
  if (Meteor.userId()) {
    if (id === 'all') {
      const firstRecord = Projects.findOne();
      if (firstRecord) return Projects.find({ _id: firstRecord._id });
    }
    return Projects.find({ _id: id });
  }
  return [];
});
