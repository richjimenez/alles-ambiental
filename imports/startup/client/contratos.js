
const Contratos = {
  fisico: (folio, venta, cliente, sf, np, tp, total, formaPago, direccionCliente, fechaEntrega, kwhp, today, project) => {
    let inversores = '';
    project.inversores.forEach((inv) => {
      inversores += `<li>${inv.cantidad} Inversor/es ${inv.nombre}</li>`;
    });
    return `
      <p style="text-align: right"><strong>Folio: ${project.folio}</strong></p>
          
      <p><strong>CONTRATO DE COMPRAVENTA DE BIENES MUEBLES QUE CELEBRAN POR UNA PARTE LA EMPRESA ${venta} 
          REPRESENTADA EN ESTE ACTO POR LA SEÑORA JAZMIN HERNANDEZ LOPEZ EN LO SUCESIVO DENOMINADA COMO “EL VENDEDOR” Y
          POR LA OTRA PARTE ${project.client.nombre.toUpperCase()} ${project.client.apellidos.toUpperCase()} 
          EN LO SUCESIVO DENOMINADA “EL COMPRADOR”, AL TENOR DE LAS
          SIGUIENTES DECLARACIONES Y CLAUSULAS.</strong></p>
  
      <div class="text-center">
        <p><strong>D E C L A R A C I O N E S:</strong></p>
      </div>
  
      <ol type="I">
        <li>Declara “EL VENDEDOR” por conducto de su representante:
          <ol type="A">
            <li>Ser una sociedad mercantil organizada y existente de conformidad con las Leyes de la República
              Mexicana, según
              se hace constar en la Escritura Pública Número 17023, de fecha 22 de Julio de 2009, otorgada ante la fe
              pública
              del Licenciado Felipe Vazquez Martin, Notario Público 03 de Guadalajara Jalisco, Jalisco, e inscrita en
              el
              Registro Público de Comercio de la ciudad de Arandas, Jalisco.
            </li>
            <li>Que su representante se encuentra legalmente envestido para celebrar el presente contrato y obligar a
              su
              representada en los términos y condiciones del mismo, según se desprende del contenido de la escritura
              pública
              citada en el inciso inmediato anterior, facultades que a la fecha no le han sido limitadas ni revocadas.
            </li>
            <li>Que tiene como objeto social entre otros, la comercialización de equipos fotovoltaicos de generación
              de energía
              eléctrica.
            </li>
            <li>Que cuenta con los recursos humanos, técnicos y económicos para hacer frente a sus obligaciones
              contraídas por
              virtud del presente instrumento jurídico.
            </li>
          </ol>
        </li><br>
        <li>Declara “EL COMPRADOR”:
          <ol type="A">
            <li>Ser una persona fisica :</li>
            <li>Que desea comprar un sistema fotovoltaico para interconectarse a la red de Comisión Federal de
              Electricidad o
              CFE (en adelante denominado “EL EQUIPO”), que le ofrece “EL VENDEDOR”, por lo que manifiesta que es su
              voluntad
              e interés celebrar el presente contrato de compraventa.
            </li>
            <li>Que cuenta con los medios económicos suficientes para hacer frente con las obligaciones de pago a que
              se
              compromete a través de la suscripción del presente instrumento.
            </li>
          </ol>
        </li><br>
        <li>Declaran ambas partes:
          <ol type="A">
            <li>Que cuentan con la capacidad jurídica necesaria para celebrar el presente contrato, así como que
              conocen
              ampliamente su alcance y consecuencias legales y el contenido de todas y cada una de las disposiciones
              legales
              que le son aplicables.
            </li>
            <li>Que es su firme intención suscribir el presente documento, para lo cual ambas partes expresan haber
              venido bajo
              su propia voluntad y con pleno conocimiento para la celebración de este negocio jurídico. De igual
              manera
              reconocen que no existe dolo, error, mala fe, reticencia o vicio alguno de la voluntad de ninguno de los
              contratantes que invalide la validez y celebración del presente contrato.
            </li>
          </ol>
        </li><br>
      </ol>
  
      <p>
        Por virtud de lo anteriormente declarado, ambas partes han convenido suscribir el presente contrato de
        compraventa al tenor de las siguientes:
      </p>
  
  
      <p class="text-center"><strong>C L A U S U L A S:</strong></p>
  
      <p>
        <strong>PRIMERA.- OBJETO DEL CONTRATO.-</strong> La compraventa de un SISTEMA FOTOVOLTAICO interconectado a
        la red de la CFE con
        una capacidad instalada de ${sf} kWp con todos sus accesorios y componentes que permitan su uso e instalación,
        el cual consiste en:
      </p>
  
      <ul>
        <li>${np} paneles solares policristalinos de ${project.panel.watts}WP, marca ${project.panel.nombre}</li>
        ${inversores}
        <li>1 estructura de aluminio para fijar ${np} paneles de ${project.panel.watts} watts marca SCHLETTER</li>
        <li>1 instalación eléctrica y materiales requeridos para la construcción de esta obra.</li>
        <li>Medidor bidireccional para tarifa comercial, CFE suministra.</li>
      </ul>
  
      <p><strong>SEGUNDA.- VALOR DEL EQUIPO INSTALACIÓN Y FORMA DE PAGO.-</strong> VALOR DEL EQUIPO ${total} el cual equivale
        al total de ${sf} Kwp instalados IVA incluido.</p>
  
      <p>El pago se hará a través ${formaPago}</p>
  
      <ul>
        <li>LUGAR DE INSTALACIÓN: En las instalaciones de “EL COMPRADOR” ubicados en ${project.client.direccion.completa}. sobre estructura 
        de aluminio soportada en techo de mismo domicilio.
        </li>
        <li>GESTION ANTE LA CFE PARA INTERCONEXIÓN: El vendedor asesorará y realizará la elaboración del contrato de
          interconexión al comprador. El tiempo de interconexión lo determina la CFE después de recibir el contrato de
          interconexión.
        </li>
        <li>El medidor lo surte la CFE una vez firmado y aprobado el contrato de interconexión.</li>
      </ul>
      <p>
        Los montos señalados en el apartado anterior serán depositados en la cuenta bancaria de “EL VENDEDOR”, a través
        de depósito bancario en la cuenta 65 507 06773-9 de Banco Santander o a través de transferencia electrónica en
        la CLABE INTERBANCARIA 014320655070677393, cuando el pago sea en pesos, cuando el pago sea en dólares, será a la
        cuenta 8250090269-5 con clabe interbancaria 014320825009026952, banco santander fungiendo como recibo del pago
        la ficha de depósito o la orden de transferencia, la cual se hará del conocimiento de “EL VENDEDOR” a través de
        fax al número telefónico 3336293150 o bien mediante correo electrónico enviado al correo lulu@allessolar.com.
      </p>
  
      <p>
        Por su parte “EL VENDEDOR” se obliga a expedir la factura correspondiente, por concepto de la compra de “EL
        EQUIPO”, materia del presente contrato, atendiendo a los ordenamientos fiscales aplicables.
      </p>
  
      <p>
        <strong>TERCERA.- PLAZO DE ENTREGA DE “EL EQUIPO”.-</strong> “EL VENDEDOR” se compromete a entregar y dejar
        debidamente
        instalado y funcionando “EL EQUIPO” el día ${fechaEntrega}, en los términos descritos en la CLAUSULA
        PRIMERA. Lo anterior sin contemplar el tiempo que tarde en realizar la interconexión la CFE., Comprometiéndose
        el vendedor a apoyar “EL CLIENTE” en la realización de todas las gestiones necesarias ante la CFE para obtener
        la interconexión.
      </p>
  
      <p>
        <strong>CUARTA.- GARANTÍA DE “EL EQUIPO”.-</strong> Las garantías en cuanto a producción de energía y vida útil,
        y eficiencia de “EL EQUIPO”, será la siguiente:
      </p>
  
      <p>
        La producción de energía de este sistema será de ${kwhp} kWh mensual promedio, viéndose modificada en la eficiencia
        en cuanto a producción de energía, en función del tiempo que transcurra en la vida de los paneles, garantizando
        que entre el año 1 y el año 12 no disminuirá del 90% su eficiencia y entre el año 12 y el año 25 no tendrán una
        eficiencia menor al 80%. En caso de que “EL EQUIPO” no generase esta cantidad de KWH +/- 5% en los tiempos
        descritos con anterioridad “EL VENDEDOR” se compromete a instalar el equipo necesario para cubrir esa
        variación.
      </p>
  
      <p>
        “EL VENDEDOR” únicamente se compromete a que “EL EQUIPO” producirá la cantidad de energía en Kwh determinadas en
        el presente instrumento.
      </p>
  
      <p>
        La vida útil y eficiencia de los paneles solares policristalinos tendrá como base 25 años teniendo una
        eficiencia mínima del 90% los primeros 12 años y después de 12 años un día a 25 años una eficiencia mínima de
        80%.
      </p>
  
      <p><strong>Inversores.</strong> Los inversores tendrán una garantía de 10 años.</p>
      <p><strong>Estructuras de aluminio.</strong> La garantía en vida útil con desgaste natural será de 20 años.</p>
      <p><strong>Instalación Eléctrica.</strong> Tendrá una garantía de materiales y construcción de 18 meses.</p>
      <p>
        Para el supuesto de fallas o desperfectos ocasionados por terceros, por “EL COMPRADOR”, o causas naturales
        ajenas a la garantía otorgada por “EL VENDEDOR”, éste se compromete a proveer a “EL COMPRADOR” la solución
        necesaria para reparar “EL EQUIPO”, a la brevedad posible con personal calificado, con costo a cargo de “EL
        COMPRADOR”.
      </p>
  
      <p>
        <strong>DOMICILIOS</strong><br>
        “EL COMPRADOR” en ${project.client.direccion.completa}
        “EL VENDEDOR” Calzada Central 45-1, Colonia Ciudad Granja, en Zapopan Jalisco
      </p>
  
      <p>
        Las partes podrán designar nuevos domicilios, previo aviso por escrito a la otra parte, en los términos
        establecidos en líneas precedentes. Dicho cambio surtirá efectos al primer día natural siguiente a la
        confirmación de recepción del aviso correspondiente.
      </p>
  
      <p>
        <strong>SEXTA.- LEY APLICABLE Y JURISDICCIÓN.-</strong> Para cualquier controversia que se derive o surja de la
        aplicación,
        cumplimiento o incumplimiento de las obligaciones asumidas por las partes en el presente contrato, ambas partes
        se someten expresamente a la jurisdicción de los Tribunales competentes de la ciudad de Guadalajara, Jalisco,
        renunciando expresamente a cualquier otra jurisdicción que por razón del domicilio o cualquier otro concepto
        pudiera corresponderles ahora o en el futuro.
      </p>
  
      <p><strong>SÉPTIMA. - SUPUESTOS DE RESCISIÓN.</strong></p>
  
      <ol>
        <li>Que “EL COMPRADOR”, no realice los pagos a su cargo en las fechas designadas para tal efecto en el presente
          contrato.</li>
        <li>Que “EL COMPRADOR”, no realice las obras de ingeniería civil a su cargo para llevar a cabo la instalación de
          “EL
          EQUIPO”.</li>
        <li>Que “EL VENDEDOR”, no haga la entrega de “EL EQUIPO” debidamente instalado en el tiempo pactado en líneas
          precedentes.</li>
      </ol>
  
      <p>
        Leído que fue el presente por las partes y enterados plenamente de su contenido, alcance y efectos legales, lo
        firman por duplicado ante la presencia de dos testigos el ${today} en la ciudad de Zapopan Jalisco,
        México.
      </p>
  
      <p>ALLES AMBIENTAL S.A. DE. C.V.<br>
      JAZMIN HERNANDEZ LOPEZ</p>
      
      <p>CLIENTE<br>
      ${cliente}</p>
    `;
  },
  moral: (folio, venta, cliente, razonSocial, sf, np, tp, total, formaPago, direccionCliente, fechaEntrega, kwhp, today, project) => {
    let inversores = '';
    project.inversores.forEach((inv) => {
      inversores += `<li>${inv.cantidad} Inversor/es ${inv.nombre}</li>`;
    });
    return `

       <p style="text-align: right"><strong>Folio: ${folio}</strong></p>
       
       <p>CONTRATO DE COMPRAVENTA DE BIENES MUEBLES QUE CELEBRAN POR UNA PARTE LA EMPRESA SOLAR INTERCOM S.A.P.I. DE C.V.
       REPRESENTADA EN ESTE ACTO POR EL SEÑOR ALFREDO HERNANDEZ HERNANDEZ, EN LO SUCESIVO DENOMINADA COMO “EL VENDEDOR” 
       Y POR LA OTRA PARTE COMPAÑÍA ${project.empresa.razon_social.toUpperCase()}, REPRESENTADA EN ESTE ACTO POR 
       ${project.client.nombre.toUpperCase()} ${project.client.apellidos.toUpperCase()}, EN LO 
       SUCESIVO DENOMINADA “EL COMPRADOR”, AL TENOR DE LAS SIGUIENTES DECLARACIONES Y CLAUSULAS.</p>
       
       <ol type="I">
        <li>Declara “EL VENDEDOR” por conducto de su representante:
          <ol type="A">
            <li>Ser una sociedad mercantil organizada y existente de conformidad con las Leyes de la República
              Mexicana, según
              se hace constar en la Escritura Pública Número 17023, de fecha 22 de Julio de 2009, otorgada ante la fe
              pública
              del Licenciado Felipe Vazquez Martin, Notario Público 03 de Guadalajara Jalisco, Jalisco, e inscrita en
              el
              Registro Público de Comercio de la ciudad de Arandas, Jalisco.
            </li>
            <li>Que su representante se encuentra legalmente envestido para celebrar el presente contrato y obligar a
              su
              representada en los términos y condiciones del mismo, según se desprende del contenido de la escritura
              pública
              citada en el inciso inmediato anterior, facultades que a la fecha no le han sido limitadas ni revocadas.
            </li>
            <li>Que tiene como objeto social entre otros, la comercialización de equipos fotovoltaicos de generación
              de energía
              eléctrica.
            </li>
            <li>Que cuenta con los recursos humanos, técnicos y económicos para hacer frente a sus obligaciones
              contraídas por
              virtud del presente instrumento jurídico.
            </li>
          </ol>
        </li><br>
        <li>Declara “EL COMPRADOR”:
          <ol type="A">
            <li>
              Ser una sociedad mercantil organizada y existente de conformidad con las Leyes de la República Mexicana, 
              según se hace constar en la Escritura Pública Número 10,326, de fecha 17 de febrero del año 2000, otorgada 
              ante la fe pública de la Licenciada Ernestina León Rodríguez, Notario Público 29 de los del Estado de 
              Aguascalientes, e inscrita en el Registro Público de Comercio de la Ciudad.
            </li>
            <li>  
              Que su representante se encuentra legalmente envestido para celebrar el presente contrato y obligar a su 
              representada en los términos y condiciones del mismo, según se desprende del contenido de la escritura 
              pública descrita en la declaración inmediata anterior, facultades que a la fecha no le han sido limitadas 
              ni revocadas.
            </li>
            <li>
              Que tiene como objeto social entre otros, la comercialización de equipos fotovoltaicos de generación de 
              energía eléctrica.
            </li>
            <li>
              Que cuenta con los recursos humanos, técnicos y económicos para hacer frente a sus 
              obligaciones contraídas por virtud del presente instrumento jurídico
            </li>
          </ol>
        </li><br>
        <li>Declaran ambas partes:
          <ol type="A">
            <li>Que cuentan con la capacidad jurídica necesaria para celebrar el presente contrato, así como que
              conocen
              ampliamente su alcance y consecuencias legales y el contenido de todas y cada una de las disposiciones
              legales
              que le son aplicables.
            </li>
            <li>Que es su firme intención suscribir el presente documento, para lo cual ambas partes expresan haber
              venido bajo
              su propia voluntad y con pleno conocimiento para la celebración de este negocio jurídico. De igual
              manera
              reconocen que no existe dolo, error, mala fe, reticencia o vicio alguno de la voluntad de ninguno de los
              contratantes que invalide la validez y celebración del presente contrato.
            </li>
          </ol>
        </li><br>
      </ol>
      <p>
        Por virtud de lo anteriormente declarado, ambas partes han convenido suscribir el presente contrato de
        compraventa al tenor de las siguientes:
      </p>
  
  
      <p class="text-center"><strong>C L A U S U L A S:</strong></p>
  
      <p>
        <strong>PRIMERA.- OBJETO DEL CONTRATO.-</strong> La compraventa de un SISTEMA FOTOVOLTAICO interconectado a
        la red de la CFE con
        una capacidad instalada de ${sf} kWp con todos sus accesorios y componentes que permitan su uso e instalación,
        el
        cual consiste en:
      </p>
  
      <ul>
        <li>${np} paneles solares policristalinos de ${project.panel.watts}WP, marca ${project.panel.nombre}</li>
        ${inversores}
        <li>1 estructura de aluminio para fijar ${np} paneles de ${project.panel.watts} watts marca SCHLETTER</li>
        <li>1 instalación eléctrica y materiales requeridos para la construcción de esta obra.</li>
        <li>Medidor bidireccional para tarifa comercial, CFE suministra.</li>
      </ul>
  
      <p><strong>SEGUNDA.- VALOR DEL EQUIPO INSTALACION Y FORMA DE PAGO.-</strong> VALOR DEL EQUIPO ${total} el cual equivale
        al total de ${sf} Kwp instalados IVA incluido.</p>
  
      <p>El pago se hará a través ${formaPago}</p>
  
      <ul>
        <li>LUGAR DE INSTALACION: En las instalaciones de “EL COMPRADOR” ubicados en ${project.client.direccion.completa}. sobre estructura 
        de aluminio soportada en techo de mismo domicilio.
        </li>
        <li>GESTION ANTE LA CFE PARA INTERCONEXION: El vendedor asesorará y realizará la elaboración del contrato de
          interconexión al comprador. El tiempo de interconexión lo determina la CFE después de recibir el contrato de
          interconexión.
        </li>
        <li>El medidor lo surte la CFE una vez firmado y aprobado el contrato de interconexión.</li>
      </ul>
      <p>
        Los montos señalados en el apartado anterior serán depositados en la cuenta bancaria de “EL VENDEDOR”, a través
        de depósito bancario en la cuenta 65 507 06773-9 de Banco Santander o a través de transferencia electrónica en
        la CLABE INTERBANCARIA 014320655070677393, cuando el pago sea en pesos, cuando el pago sea en dólares, será a la
        cuenta 8250090269-5 con clabe interbancaria 014320825009026952, banco santander fungiendo como recibo del pago
        la ficha de depósito o la orden de transferencia, la cual se hará del conocimiento de “EL VENDEDOR” a través de
        fax al número telefónico 3336293150 o bien mediante correo electrónico enviado al correo lulu@allessolar.com.
      </p>
  
      <p>
        Por su parte “EL VENDEDOR” se obliga a expedir la factura correspondiente, por concepto de la compra de “EL
        EQUIPO”, materia del presente contrato, atendiendo a los ordenamientos fiscales aplicables.
      </p>
  
      <p>
        <strong>TERCERA.- PLAZO DE ENTREGA DE “EL EQUIPO”.-</strong> “EL VENDEDOR” se compromete a entregar y dejar
        debidamente
        instalado y funcionando “EL EQUIPO” el día ${fechaEntrega}, en los términos descritos en la CLAUSULA
        PRIMERA. Lo anterior sin contemplar el tiempo que tarde en realizar la interconexión la CFE., Comprometiendose
        el vendedor a apoyar “EL CLIENTE” en la realización de todas las gestiones necesarias ante la CFE para obtener
        la interconexión.
      </p>
  
      <p>
        <strong>CUARTA.- GARANTÍA DE “EL EQUIPO”.-</strong> Las garantías en cuanto a producción de energía y vida útil,
        y eficiencia de “EL EQUIPO”, será la siguiente:
      </p>
  
      <p>
        La producción de energía de este sistema será de ${kwhp} kWh mensual promedio, viéndose modificada en la eficiencia
        en cuanto a producción de energía, en función del tiempo que transcurra en la vida de los paneles, garantizando
        que entre el año 1 y el año 12 no disminuirá del 90% su eficiencia y entre el año 12 y el año 25 no tendrán una
        eficiencia menor al 80%. En caso de que “EL EQUIPO” no generase esta cantidad de KWH +/- 5% en los tiempos
        descritos con anterioridad “EL VENDEDOR” se compromete a instalar el quipo necesario para cubrir esa
        variación.
      </p>
  
      <p>
        “EL VENDEDOR” únicamente se compromete a que “EL EQUIPO” producirá la cantidad de energía en Kwh determinadas en
        el presente instrumento.
      </p>
  
      <p>
        La vida útil y eficiencia de los paneles solares policristalinos tendrá como base 25 años teniendo una
        eficiencia mínima del 90% los primeros 12 años y después de 12 años un día a 25 años una eficiencia mínima de
        80%.
      </p>
  
      <p><strong>Inversores.</strong> Los inversores tendrán una garantía de 10 años.</p>
      <p><strong>Estructuras de aluminio.</strong> La garantía en vida útil con desgaste natural será de 20 años.</p>
      <p><strong>Instalación Eléctrica.</strong> Tendrá una garantía de materiales y construcción de 18 meses.</p>
      <p>
        Para el supuesto de fallas o desperfectos ocasionados por terceros, por “EL COMPRADOR”, o causas naturales
        ajenas a la garantía otorgada por “EL VENDEDOR”, éste se compromete a proveer a “EL COMPRADOR” la solución
        necesaria para reparar “EL EQUIPO”, a la brevedad posible con personal calificado, con costo a cargo de “EL
        COMPRADOR”.
      </p>
  
      <p>
        <strong>DOMICILIOS</strong><br>
        “EL COMPRADOR” en ${project.client.direccion.completa}
        “EL VENDEDOR” Calzada Central 45-1, Colonia Ciudad Granja, en Zapopan Jalisco
      </p>
  
      <p>
        Las partes podrán designar nuevos domicilios, previo aviso por escrito a la otra parte, en los términos
        establecidos en líneas precedentes. Dicho cambio surtirá efectos al primer día natural siguiente a la
        confirmación de recepción del aviso correspondiente.
      </p>
  
      <p>
        <strong>SEXTA.- LEY APLICABLE Y JURISDICCIÓN.-</strong> Para cualquier controversia que se derive o surja de la
        aplicación,
        cumplimiento o incumplimiento de las obligaciones asumidas por las partes en el presente contrato, ambas partes
        se someten expresamente a la jurisdicción de los Tribunales competentes de la ciudad de Guadalajara, Jalisco,
        renunciando expresamente a cualquier otra jurisdicción que por razón del domicilio o cualquier otro concepto
        pudiera corresponderles ahora o en el futuro.
      </p>
  
      <p><strong>SEPTIMA. - SUPUESTOS DE RESCISION.</strong></p>
  
      <ol>
        <li>Que “EL COMPRADOR”, no realice los pagos a su cargo en las fechas designadas para tal efecto en el presente
          contrato.</li>
        <li>Que “EL COMPRADOR”, no realice las obras de ingeniería civil a su cargo para llevar a cabo la instalación de
          “EL
          EQUIPO”.</li>
        <li>Que “EL VENDEDOR”, no haga la entrega de “EL EQUIPO” debidamente instalado en el tiempo pactado en líneas
          precedentes.</li>
      </ol>
  
      <p>
        Leído que fue el presente por las partes y enterados plenamente de su contenido, alcance y efectos legales, lo
        firman por duplicado ante la presencia de dos testigos el ${today} en la ciudad de Zapopan Jalisco,
        México.
      </p>
  
      <p>${venta}<br>
      ALFREDO HERNANDEZ HERNANDEZ</p>
      
      <p>
      ${project.empresa.razon_social.toUpperCase()} <br> 
      ${project.client.nombre.toUpperCase()} ${project.client.apellidos.toUpperCase()}
      </p>
    `;
  },
};
export { Contratos as default };
