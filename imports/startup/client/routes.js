import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

// Global
import '../../ui/pages/not-found/not-found';

// Admin Layouts
import '/imports/ui/layouts/admin/body/body';
import '/imports/ui/layouts/admin/login/login';
import '/imports/ui/layouts/contratos/contratos';

// Public Layouts
import '/imports/ui/layouts/public/body/body';

// Admin Components
import '/imports/ui/components/admin/header/header';
import '/imports/ui/components/admin/sidebar/sidebar';
import '/imports/ui/components/admin/footer/footer';

// Public Components
import '/imports/ui/components/public/header/header';
import '/imports/ui/components/public/footer/footer';

// Admin Pages
import './routes/admin/login';
import './routes/admin/users';
import './routes/admin/clientes';
import './routes/admin/projects';
import './routes/admin/reports';
import './routes/admin/zones';
import './routes/admin/catalog';
import './routes/admin/settings';
import './routes/admin/contratos';

// Public Pages
import '/imports/ui/pages/public/home/home';

BlazeLayout.setRoot('body');

// root
FlowRouter.route('/', {
  triggersEnter: [
    (context, redirect) => {
      if (Meteor.userId()) redirect('/admin/projects/list/all');
      else redirect('/login');
    },
  ],
  name: 'public.home',
});

FlowRouter.notFound = {
  name: 'not.found',
  action() {
    BlazeLayout.render('adminBody', {
      header: 'menu',
      main: 'App_notFound',
      footer: 'footer',
    });
  },
};
