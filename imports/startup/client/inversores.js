import log from 'loglevel';

const between = (x, min, max) => x >= min && x <= max;
const Inversores = {
  get: (pago, nP, watts, voltaje) => {
    log.debug(pago);
    log.debug(nP);
    log.debug(watts);
    log.debug(voltaje);
    // nP = numero de paneles, nI = numero de inversores
    if (pago === 'contado') {
      if (between(nP, 2, 6) && watts === 275) {
        if (voltaje === 127 || voltaje === 220) {
          return [{ cantidad: 1, nombre: 'Micro inversor APS 500w' }];
        }
      }
      if (between(nP, 6, 8) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 1, nombre: 'SMA 3.8 kw' }];
      }
    }
    if (pago === 'contado' || pago === 'credito') {
      if (between(nP, 9, 12 && voltaje === 220)) {
        if (watts === 330 || watts === 370) {
          return [{ cantidad: 1, nombre: 'Fronius 5.0 kw' }];
        }
      }
      if (between(nP, 13, 18) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 1, nombre: 'Fronius 5.0 kw o SMA 5.0 kw' }];
      }
      if (between(nP, 13, 16) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 1, nombre: 'Fronius 5.0 kw o SMA 5.0 kw' }];
      }
      if (between(nP, 19, 26) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 1, nombre: 'Fronius 7.6 kw o SMA 7.7 kw' }];
      }
      if (between(nP, 17, 26) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 1, nombre: 'Fronius 7.6 kw o SMA 7.7 kw' }];
      }
      if (between(nP, 27, 36) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 2, nombre: 'Fronius 5.0 kw o SMA 5.0 kw' }];
      }
      if (between(nP, 27, 32) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 2, nombre: 'Fronius 5.0 kw o SMA 5.0 kw' }];
      }
      if (between(nP, 37, 56) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 1, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 34, 48) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 1, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 57, 112) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 2, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 49, 96) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 2, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 113, 130) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 3, nombre: '2 Fronius 15.0 kw y 1 Fronius 5.0 kw' }];
      }
      if (between(nP, 97, 112) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 3, nombre: '2 Fronius 15.0 kw y 1 Fronius 5.0 kw' }];
      }
      if (between(nP, 131, 138) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 3, nombre: '2 Fronius 15.0 kw y 1 Fronius 7.6 kw' }];
      }
      if (between(nP, 113, 122) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 3, nombre: '2 Fronius 15.0 kw y 1 Fronius 5.0 kw' }];
      }
      if (between(nP, 139, 168) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 3, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 123, 144) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 3, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 169, 224) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 4, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 145, 192) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 4, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 225, 280) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 5, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 193, 240) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 5, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 281, 336) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 6, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 241, 288) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 6, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 337, 392) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 7, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 289, 336) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 7, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 393, 448) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 8, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 337, 384) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 8, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 449, 504) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 9, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 385, 432) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 9, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 505, 560) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 10, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 433, 480) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 10, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 561, 616) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 11, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 481, 528) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 11, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 617, 672) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 12, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 529, 576) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 12, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 673, 728) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 13, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 577, 624) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 13, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 729, 784) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 14, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 625, 672) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 14, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 785, 840) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 15, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 673, 720) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 15, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 841, 896) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 16, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 721, 768) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 16, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 897, 952) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 17, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 769, 816) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 17, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 953, 1008) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 18, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 817, 864) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 18, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1009, 1064) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 19, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 865, 912) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 19, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1065, 1120) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 20, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 913, 960) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 20, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1121, 1176) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 21, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 961, 1008) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 21, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1177, 1232) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 22, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1009, 1056) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 22, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1233, 1288) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 23, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1057, 1104) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 23, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1289, 1344) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 24, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1105, 1152) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 24, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1345, 1400) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 25, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1153, 1200) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 25, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1401, 1456) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 26, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1201, 1248) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 26, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1457, 1512) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 27, nombre: 'Fronius 15.0 kw' }];
      }
      if (between(nP, 1249, 1296) && watts === 370 && voltaje === 220) {
        return [{ cantidad: 27, nombre: 'Fronius 15.0 kw' }];
      }
      // 440
      if (between(nP, 60, 90) && watts === 330 && voltaje === 440) {
        return [{ cantidad: 1, nombre: 'Fronius 24.0 kw o SMA 24.0 kw' }];
      }
      if (between(nP, 60, 90) && watts === 330 && voltaje === 440) {
        return [{ cantidad: 1, nombre: 'Fronius 24.0 kw o SMA 24.0 kw' }];
      }
      if (between(nP, 54, 80) && watts === 370 && voltaje === 440) {
        return [{ cantidad: 1, nombre: 'Fronius 24.0 kw o SMA 24.0 kw' }];
      }
      if (between(nP, 91, 180) && watts === 330 && voltaje === 440) {
        return [{ cantidad: 2, nombre: 'Fronius 24.0 kw o SMA 24.0 kw' }];
      }
      if (between(nP, 81, 169) && watts === 370 && voltaje === 440) {
        return [{ cantidad: 2, nombre: 'Fronius 24.0 kw o SMA 24.0 kw' }];
      }
      if (between(nP, 181, 342) && watts === 330 && voltaje === 440) {
        return [{ cantidad: 1, nombre: 'SMA core 1 62.0 kw' }];
      }
      if (between(nP, 170, 342) && watts === 370 && voltaje === 440) {
        return [{ cantidad: 1, nombre: 'SMA core 1 62.0 kw' }];
      }
      if (between(nP, 343, 684) && watts === 330 && voltaje === 440) {
        return [{ cantidad: 2, nombre: 'SMA core 1 62.0 kw' }];
      }
      if (between(nP, 343, 684) && watts === 370 && voltaje === 440) {
        return [{ cantidad: 2, nombre: 'SMA core 1 62.0 kw' }];
      }
      if (between(nP, 685, 1026) && watts === 330 && voltaje === 440) {
        return [{ cantidad: 3, nombre: 'SMA core 1 62.0 kw' }];
      }
      if (between(nP, 685, 1026) && watts === 370 && voltaje === 440) {
        return [{ cantidad: 3, nombre: 'SMA core 1 62.0 kw' }];
      }
      if (between(nP, 1027, 1368) && watts === 330 && voltaje === 440) {
        return [{ cantidad: 4, nombre: 'SMA core 1 62.0 kw' }];
      }
      if (between(nP, 1027, 1368) && watts === 370 && voltaje === 440) {
        return [{ cantidad: 4, nombre: 'SMA core 1 62.0 kw' }];
      }
      if (between(nP, 1369, 1515) && watts === 330 && voltaje === 440) {
        return [{ cantidad: 5, nombre: 'SMA core 1 62.0 kw' }];
      }
      if (between(nP, 1369, 1515) && watts === 370 && voltaje === 440) {
        return [{ cantidad: 5, nombre: 'SMA core 1 62.0 kw' }];
      }
    }
    if (pago === 'credito') {
      if (between(nP, 53, 54) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 3, nombre: 'Fronius 5.0 kw o SMA 5.0 kw' }];
      }
      if (between(nP, 55, 64) && watts === 330 && voltaje === 220) {
        return [{ cantidad: 3, nombre: '2 Fronius 7.6 kw y Fronius 3.8 kw o 2 SMA 7.7 kw y SMA 3.8 kw ' }];
      }
    }

    // if (pago === 'contado' || pago === 'ecoCredito') {
    //   if (between(nP, 4, 12) && voltaje === 127 && watts === 275) {
    //     let nI = 0;
    //     if (nP % 2 === 1) nI = (nP - 1) / 2;
    //     else nI = nP / 2;
    //     return [{ cantidad: nI, nombre: 'Microinversor APS 500 w' }];
    //   }
    //   if (between(nP, 9, 12) && watts === 275 && voltaje === 220) {
    //     return [{ cantidad: 1, nombre: 'SMA 3.8 kw' }];
    //   }
    //   if (between(nP, 9, 12) && voltaje === 220) {
    //     if (watts === 330 || watts === 370) return [{ cantidad: 1, nombre: 'SMA 3.8 kw' }];
    //   }
    //   if (between(nP, 13, 18) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 1, nombre: 'Fronius 5.0 kw' }];
    //   }
    //   if (between(nP, 13, 16) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 1, nombre: 'Fronius 5.0 kw' }];
    //   }
    //   if (between(nP, 19, 26) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 1, nombre: 'Fronius 7.6 kw' }];
    //   }
    //   if (between(nP, 17, 26) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 1, nombre: 'Fronius 7.6 kw' }];
    //   }
    //   if (between(nP, 27, 36) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 2, nombre: 'Fronius 5.0 kw' }];
    //   }
    //   if (between(nP, 27, 32) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 2, nombre: 'Fronius 5.0 kw' }];
    //   }
    //   if (between(nP, 36, 38) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 2, nombre: 'SMA 3.8 kw' }, { cantidad: 1, nombre: 'Fronius 7.6 kw' }];
    //   }
    //   if (between(nP, 33, 38) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 2, nombre: 'SMA 3.8 kw' }, { cantidad: 1, nombre: 'Fronius 7.6 kw' }];
    //   }
    //   if (between(nP, 39, 52) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 2, nombre: 'Fronius 7.6 kw' }];
    //   }
    //   if (between(nP, 34, 52) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 2, nombre: 'Fronius 7.6 kw' }];
    //   }
    // }
    // if (pago === 'ecoCredito') {
    //   if (between(nP, 53, 54) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 3, nombre: 'Fronius 5.0 kw' }];
    //   }
    //   if (between(nP, 55, 64) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 1, nombre: 'SMA 3.8 kw' }, { cantidad: 2, nombre: 'Fronius 7.6 kw' }];
    //   }
    //   if (between(nP, 53, 64) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 1, nombre: 'SMA 3.8 kw' }, { cantidad: 2, nombre: 'Fronius 7.6 kw' }];
    //   }
    //   if (between(nP, 65, 70) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 1, nombre: 'Fronius 5.0 kw' }, { cantidad: 2, nombre: 'Fronius 7.6 kw' }];
    //   }
    //   if (between(nP, 65, 68) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 1, nombre: 'Fronius 5.0 kw' }, { cantidad: 2, nombre: 'Fronius 7.6 kw' }];
    //   }
    //   if (between(nP, 71, 78) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 3, nombre: 'Fronius 7.6 kw' }];
    //   }
    //   if (between(nP, 69, 78) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 3, nombre: 'Fronius 7.6 kw' }];
    //   }
    // }
    // if (pago === 'contado') {
    //   if (between(nP, 38, 56) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 1, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 34, 48) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 1, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 57, 112) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 2, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 49, 96) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 2, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 113, 130) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 2, nombre: 'Fronius 15.0' }, { cantidad: 1, nombre: 'Fronius 5.0 kw' }];
    //   }
    //   if (between(nP, 97, 112) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 2, nombre: 'Fronius 15.0' }, { cantidad: 1, nombre: 'Fronius 5.0 kw' }];
    //   }
    //   if (between(nP, 131, 138) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 2, nombre: 'Fronius 15.0' }, { cantidad: 1, nombre: 'Fronius 7.6 kw' }];
    //   }
    //   if (between(nP, 113, 122) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 2, nombre: 'Fronius 15.0' }, { cantidad: 1, nombre: 'Fronius 7.6 kw' }];
    //   }
    //   if (between(nP, 139, 164) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 2, nombre: 'Fronius 15.0' }, { cantidad: 2, nombre: 'Fronius 7.6 kw' }];
    //   }
    //   if (between(nP, 123, 148) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 2, nombre: 'Fronius 15.0' }, { cantidad: 2, nombre: 'Fronius 7.6 kw' }];
    //   }
    //   if (between(nP, 165, 168) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 3, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 169, 224) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 4, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 149, 192) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 4, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 225, 280) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 5, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 193, 240) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 5, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 281, 336) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 6, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 241, 288) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 6, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 337, 392) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 7, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 289, 336) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 7, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 393, 448) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 8, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 337, 384) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 8, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 449, 504) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 9, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 385, 432) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 9, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 505, 560) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 10, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 433, 480) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 10, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 561, 616) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 11, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 481, 528) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 11, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 617, 672) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 12, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 529, 576) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 12, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 673, 728) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 13, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 577, 624) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 13, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 729, 784) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 14, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 625, 672) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 14, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 785, 840) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 15, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 673, 720) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 15, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 841, 896) && watts === 330 && voltaje === 220) {
    //     return [{ cantidad: 16, nombre: 'Fronius 15.0' }];
    //   }
    //   if (between(nP, 721, 768) && watts === 370 && voltaje === 220) {
    //     return [{ cantidad: 16, nombre: 'Fronius 15.0' }];
    //   }
    // }

    return [{ cantidad: 0, nombre: 'No se pueden calcular la cantidad de inversores' }];
  },
};
export { Inversores as default };
