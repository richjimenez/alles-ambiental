import jsPDF from 'jspdf';
import { Meteor } from 'meteor/meteor';
import { Alert } from '/imports/startup/client/alerts';
import $ from 'jquery';
import log from 'loglevel';

const PDF = {
  generate: (data, email) => {
    log.debug(data);
    const logo = new Image();
    const footer = new Image();
    const generatePDF = () => {
      const size = { w: 215.9, h: 279.4 };
      const doc = new jsPDF({
        unit: 'mm',
        format: 'letter',
      });
      // console.log(doc.getFontList());
      doc.setFont('helvetica');
      doc.setDrawColor(0);
      doc.setFillColor('#77C043');
      doc.rect(0, 0, size.w, 6.4, 'F');
      doc.setDrawColor(0);
      doc.setFillColor('#23659A');
      doc.rect(0, 6.4, size.w, 45, 'F');

      doc.addImage(logo, 5, 8, 40, 40);

      let y1 = 18;
      doc.setFontSize(12);
      doc.setTextColor('#00ACEF');
      doc.text('Fecha:', 120, y1);
      doc.text('Forma de pago:', 160, y1);
      y1 += 6;
      doc.setFontSize(14);
      doc.setTextColor('#FFFFFF');
      doc.text(data.fecha, 120, y1);
      doc.text(data.pago, 160, y1);
      y1 += 12;
      doc.setFontSize(12);
      doc.setTextColor('#00ACEF');
      doc.text('Folio:', 120, y1);
      doc.text('Tipo de cambio:', 160, y1);
      y1 += 6;
      doc.setFontSize(14);
      doc.setTextColor('#FFFFFF');
      doc.text(data.folio, 120, y1);
      doc.text(data.cambio, 160, y1);


      const mt1 = 65;
      doc.setTextColor('#23659A');
      doc.setFontSize(22);
      doc.setFontType('bold');
      doc.text('Gracias por cotizar', 5, mt1);
      doc.text('con nosotros,', 5, mt1 + 8);
      doc.text(data.cliente.nombre, 5, mt1 + 16);

      const y2 = 90;
      doc.setFontSize(14);
      doc.setFontType('normal');
      doc.setTextColor('#00ACEF');
      doc.text('Empecemos con tu proyecto', 5, y2);
      doc.text('lo antes posible.', 5, y2 + 6);

      const y3 = 101;
      doc.setDrawColor('#23659A');
      doc.line(5, y3, 210.9, y3);

      const y4 = 108;
      doc.setFontSize(12);
      doc.setTextColor('#23659A');
      doc.text('NOMBRE:', 5, y4);
      doc.setFontType('bold');
      doc.text(`${data.cliente.nombre} ${data.cliente.apellidos}`, 26, y4);

      doc.setFontType('normal');
      doc.text('ZONA:', 5, y4 + 6);
      doc.setFontType('bold');
      doc.text(data.zona, 19, y4 + 6);

      doc.setFontType('normal');
      doc.text('NO. SERVICIO / RPU:', 5, y4 + 12);
      doc.setFontType('bold');
      doc.text(data.numero_servicio, 48, y4 + 12);

      doc.setFontType('normal');
      doc.text('TARIFA:', 5, y4 + 18);
      doc.setFontType('bold');
      doc.text(data.tarifa.nombre, 22, y4 + 18);

      const y5 = 130;
      doc.line(5, y5, 210.9, y5);

      const y6 = 134;
      doc.setFillColor('#f2f4f5');
      doc.rect(5, y6, 65.3, 12, 'F');
      doc.setFontSize(10);
      doc.setFontType('bold');
      doc.text('GENERACIÓN ENERGÍA MENSUAL', 7, y6 + 5);
      doc.setFontType('normal');
      doc.text('KWH PROMEDIO', 7, y6 + 9);

      doc.setFillColor('#f2f4f5');
      doc.rect(75.3, y6, 65.3, 12, 'F');
      doc.setFontSize(10);
      doc.setFontType('bold');
      doc.text('CONSUMO ACTUAL MENSUAL', 77.3, y6 + 5);
      doc.setFontType('normal');
      doc.text('KWH PROMEDIO', 77.3, y6 + 9);

      doc.setFillColor('#f2f4f5');
      doc.rect(145.6, y6, 65.3, 12, 'F');
      doc.setFontSize(10);
      doc.setFontType('bold');
      doc.text('AHORRO % POR', 147.6, y6 + 5);
      doc.setFontType('normal');
      doc.text('GENERACIÓN FOTOVOLTAICA', 147.6, y6 + 9);

      const y7 = 146;
      doc.setFillColor('#eaeaea');
      doc.rect(5, y7, 65.3, 20, 'F');
      doc.setFontSize(30);
      doc.setFontType('bold');
      doc.text(`${data.generacionMensual} KWH`, 7, y7 + 13);

      doc.setFillColor('#eaeaea');
      doc.rect(75.3, y7, 65.3, 20, 'F');
      doc.setFontSize(30);
      doc.setFontType('bold');
      doc.text(`${data.consumoActualMensual} KWH`, 77.3, y7 + 13);

      doc.setFillColor('#eaeaea');
      doc.rect(145.6, y7, 65.3, 20, 'F');
      doc.setFontSize(30);
      doc.setFontType('bold');
      doc.text(data.ahorroMensual, 147.6, y7 + 13);

      const y8 = 170; // 197
      doc.setFillColor('#cf6527');
      doc.rect(5, y8, 65.3, 12, 'F');
      doc.setTextColor('#FFFFFF');
      doc.setFontSize(10);
      doc.setFontType('normal');
      doc.text('PAGO ACTUAL ANUAL CFE', 7, y8 + 5);
      doc.text('SIN PANELES', 7, y8 + 9);

      doc.setFillColor('#23659A');
      doc.rect(75.3, y8, 65.3, 12, 'F');
      doc.setTextColor('#FFFFFF');
      doc.setFontSize(10);
      doc.text('PAGO REAL A CFE', 77.3, y8 + 5);
      doc.text('CON PANELES', 77.3, y8 + 9);

      doc.setFillColor('#88bc57');
      doc.rect(145.6, y8, 65.3, 12, 'F');
      doc.setTextColor('#FFFFFF');
      doc.setFontSize(10);
      doc.text('AHORRO ANUAL', 147.6, y8 + 5);

      const y9 = 182;
      doc.setFillColor('#fbe3a4');
      doc.rect(5, y9, 65.3, 20, 'F');
      doc.setTextColor('#23659A');
      doc.setFontSize(27);
      doc.setFontType('bold');
      doc.text(data.pagoActual, 7, y9 + 13);

      doc.setFillColor('#00ACEF');
      doc.rect(75.3, y9, 65.3, 20, 'F');
      doc.setTextColor('#FFFFFF');
      doc.setFontSize(27);
      doc.setFontType('bold');
      doc.text(data.pagoReal, 77.3, y9 + 13);

      doc.setFillColor('#e2f2b7');
      doc.rect(145.6, y9, 65.3, 20, 'F');
      doc.setTextColor('#23659A');
      doc.setFontSize(27);
      doc.setFontType('bold');
      doc.text(data.ahorroAnual, 147.6, y9 + 13);

      doc.line(5, y9 + 24, 210.9, y9 + 24);

      let graficasY = 223;

      doc.setFillColor('#f0f0f0');
      doc.roundedRect(50, graficasY - 5, size.w - 55, 15, 1, 1, 'F');
      doc.setFillColor('#cf6527');
      doc.roundedRect(51, graficasY - 4, (size.w - 57) * data.pGrafica1 / 100, 13, 1, 1, 'F');
      doc.setTextColor('#23659A');
      doc.setFontSize(20);
      doc.text(data.pagoActual, 208.9, graficasY + 5, { align: 'right' });

      doc.setFontSize(10);
      doc.setFontType('bold');
      doc.setTextColor('#23659A');
      doc.text('ANTES', 5, graficasY);
      doc.setFontType('normal');
      graficasY += 4;
      doc.setFontSize(9);
      doc.setTextColor('#8E918F');
      doc.text('Pago actual anual CFE', 5, graficasY);
      doc.setTextColor('#cf6527');
      graficasY += 4;
      doc.setFontType('bold');
      doc.text('SIN PANELES', 5, graficasY);

      graficasY += 10;
      doc.setFillColor('#f0f0f0');
      doc.roundedRect(50, graficasY - 5, size.w - 55, 15, 1, 1, 'F');
      doc.setFillColor('#00ACEF');
      doc.roundedRect(51, graficasY - 4, (size.w - 57) * data.pGrafica2 / 100, 13, 1, 1, 'F');
      doc.setTextColor('#23659A');
      doc.setFontSize(20);
      doc.text(data.pagoReal, 208.9, graficasY + 5, { align: 'right' });

      doc.setFontSize(10);
      doc.setTextColor('#23659A');
      doc.text('DESPUÉS', 5, graficasY);
      doc.setFontType('normal');
      graficasY += 4;
      doc.setFontSize(9);
      doc.setTextColor('#8E918F');
      doc.text('Pago actual anual CFE', 5, graficasY);
      doc.setTextColor('#00ACEF');
      graficasY += 4;
      doc.setFontType('bold');
      doc.text('CON PANELES', 5, graficasY);

      graficasY += 10;
      doc.setFillColor('#f0f0f0');
      doc.roundedRect(50, graficasY - 5, size.w - 55, 15, 1, 1, 'F');
      doc.setFillColor('#88bc57');
      doc.roundedRect(51, graficasY - 4, (size.w - 57) * data.pGrafica3 / 100, 13, 1, 1, 'F');
      doc.setFontSize(20);
      doc.setTextColor('#23659A');
      doc.text(data.ahorroAnual, 208.9, graficasY + 5, { align: 'right' });
      doc.setFontSize(10);
      doc.setTextColor('#88bc57');
      doc.text('AHORRO ANUAL', 5, graficasY);

      doc.addPage();

      let y10 = 10;

      doc.setFontSize(12);
      doc.setFontType('normal');
      doc.setTextColor('#8E918F');
      doc.text('VOLTAJE', 5, y10);
      doc.setFontSize(20);
      doc.setFontType('bold');
      doc.setTextColor('#23659A');
      y10 += 7;
      doc.text(`${data.voltaje}V`, 5, y10);

      doc.setFontSize(12);
      doc.setFontType('normal');
      doc.setTextColor('#8E918F');
      y10 += 10;
      doc.text('SISTEMA FOTOVOLTAICO', 5, y10);
      doc.setFontSize(20);
      doc.setFontType('bold');
      doc.setTextColor('#23659A');
      y10 += 7;
      doc.text(data.sistemaFoto, 5, y10);

      doc.setFontSize(12);
      doc.setFontType('normal');
      doc.setTextColor('#8E918F');
      y10 += 10;
      doc.text('SUPERFICIE', 5, y10);
      doc.setFontSize(20);
      doc.setFontType('bold');
      doc.setTextColor('#23659A');
      y10 += 7;
      doc.text(data.superficie, 5, y10);

      doc.setFontSize(12);
      doc.setFontType('normal');
      doc.setTextColor('#8E918F');
      y10 += 10;
      doc.text('NO. DE PANELES', 5, y10);
      doc.text('TIPO', 50, y10);
      doc.setFontSize(20);
      doc.setFontType('bold');
      doc.setTextColor('#23659A');
      y10 += 7;
      doc.text(data.noPaneles, 5, y10);
      doc.text(data.panel, 50, y10);

      doc.setFontSize(12);
      doc.setFontType('normal');
      doc.setTextColor('#8E918F');
      y10 += 10;
      doc.text('INVERSORES', 5, y10);
      doc.setFontSize(20);
      doc.setFontType('bold');
      doc.setTextColor('#23659A');
      y10 += 7;

      data.inversores.forEach((inversor) => {
        doc.text(`${inversor.cantidad} - ${inversor.nombre}`, 5, y10);
        y10 += 7;
      });

      doc.setDrawColor('#8E918F');
      doc.line(5, y10, 210.9, y10);
      y10 += 10;

      doc.setFontSize(12);
      doc.setFontType('normal');
      doc.setTextColor('#23659A');
      doc.text('VALOR EQUIPO FOTOVOLTAICO', 5, y10);
      doc.setFontSize(18);
      doc.setFontType('bold');
      doc.text(data.subTotal, 210.9, y10, { align: 'right' });

      y10 += 7;

      doc.setFontSize(12);
      doc.setFontType('normal');
      doc.setTextColor('#23659A');
      doc.text('IVA', 5, y10);
      doc.setFontSize(18);
      doc.setFontType('bold');
      doc.text(data.iva, 210.9, y10, { align: 'right' });

      y10 += 7;

      doc.setFontSize(12);
      doc.setFontType('normal');
      doc.setTextColor('#23659A');
      doc.text('TOTAL PESOS MEXICANOS', 5, y10);
      doc.setFontSize(20);
      doc.setFontType('bold');
      doc.text(data.total, 210.9, y10, { align: 'right' });

      y10 += 10;
      doc.setFontSize(8);
      doc.setFontType('normal');
      doc.setTextColor('#8E918F');
      doc.text('Este precio considera todos los costos relacionados con el proyecto:', 5, y10);
      y10 += 4;
      doc.setFontType('bold');
      doc.text('Panel, estructura, cableado, inversor, trámites con CFE, medidor bidireccional, instalación, mano de obra, puesta en marcha.', 5, y10);


      y10 += 7;
      doc.setDrawColor('#8E918F');
      doc.line(5, y10, 210.9, y10);
      y10 += 10;


      doc.setFontSize(10);
      doc.setTextColor('#23659A');
      doc.text('NOTAS:', 5, y10);
      y10 += 5;

      doc.setFontSize(8);
      doc.setFontType('normal');
      doc.setTextColor('#8E918F');
      doc.text('* El ahorro estimado en $ es referencial para comparativo,', 5, y10);
      doc.setFontType('bold');
      doc.text('la garantia en generación es en base a generación energía mensual KWH promedio.', 79, y10);
      y10 += 5;

      // doc.setFontType('normal');
      // doc.text('* El sistema fotovoltaico interconectado a la red, no genera ahorro en la', 5, y10);
      // doc.setFontType('bold');
      // doc.text('Demanda Maxima ni Factor de potencia.', 96, y10);
      // y10 += 5;

      doc.setFontType('normal');
      doc.text('* La garantia ofrecida por empresa es en base a', 5, y10);
      doc.setFontType('bold');
      doc.text('generación de energía mensual promedio ( kWh ).', 66, y10);
      y10 += 10;

      // doc.setFontType('normal');
      // doc.text('* En tarifas', 5, y10);
      // doc.setFontType('bold');
      // doc.text('GDMTO y GDMTH', 19, y10);
      // doc.setFontType('normal');
      // doc.text('no se genera el rubro de', 44, y10);
      // doc.setFontType('bold');
      // doc.text('Demanda Máxima ( Distribución y Capacidad ).', 76, y10);
      // y10 += 10;

      doc.setFillColor('#23659A');
      doc.rect(5, y10, 50, 8, 'F');
      doc.setFontSize(10);
      doc.setTextColor('#FFFFFF');
      doc.text('GARANTÍA ALLES SOLAR', 7, y10 + 5);
      y10 += 12;

      doc.setTextColor('#23659A');
      doc.setFontSize(8);
      doc.text('Inversor Fronius-SMA', 5, y10);
      doc.text('Estructura Schletter-Everest Aluminio (Alemana)', 5, y10 + 5);
      doc.text('Panel Solar Risen Energy ( Tier 1)', 5, y10 + 10);
      doc.text('Instalación y Soporte Técnico', 5, y10 + 15);
      doc.setFontType('normal');
      doc.text('10 años contra defecto fabricación.', 80, y10);
      doc.text('20 años.', 80, y10 + 5);
      doc.text('12 años al 90 % de eficiencia, 25 años al 80%.', 80, y10 + 10);
      doc.text('18 meses.', 80, y10 + 15);
      y10 += 25;

      doc.setFillColor('#23659A');
      doc.rect(5, y10, 50, 8, 'F');
      doc.setFontType('bold');
      doc.setFontSize(10);
      doc.setTextColor('#FFFFFF');
      doc.text('CONDICIONES DE VENTA', 7, y10 + 5);
      y10 += 12;

      doc.setTextColor('#23659A');
      doc.setFontSize(8);
      doc.text('Vigencia de cotización 30 días hábiles.', 5, y10);
      y10 += 10;

      doc.setFillColor('#23659A');
      doc.rect(5, y10, 50, 8, 'F');
      doc.setFontSize(10);
      doc.setTextColor('#FFFFFF');
      doc.text('EXPERIENCIA', 7, y10 + 5);
      y10 += 12;

      doc.setTextColor('#23659A');
      doc.setFontSize(8);
      doc.setFontType('bolditalic');
      doc.text('EMPRESA CONSTITUIDA DESDE 2009, CON EXPERIENCIA EN SECTOR DOMÉSTICO, COMERCIAL E INDUSTRIAL.', 5, y10);

      doc.addPage();

      let y11 = 5;
      doc.setFillColor('#23659A');
      doc.rect(5, y11, 50, 8, 'F');
      doc.setFontSize(10);
      doc.setFontType('bold');
      doc.setTextColor('#FFFFFF');
      doc.text('ALCANCE DEL PROYECTO', 7, y11 + 5);
      y11 += 12;

      doc.setTextColor('#23659A');
      doc.setFontSize(8);
      doc.text('INCLUYE:', 5, y11);
      y11 += 7;
      doc.setFontType('normal');
      doc.text('1. Obra sobre nave.', 5, y11);
      y11 += 7;
      doc.text('2. Instalación de equipos, material eléctrico, centros de carga para interconexión, mano de obra '
        + 'calificada, instalación de estructura,', 5, y11);
      y11 += 3;
      doc.text('estructura de aluminio marca Schletter o Everest.', 8, y11);
      y11 += 7;
      doc.text('3. Unidad de inspección en caso de requerirse.', 5, y11);
      y11 += 7;
      doc.text('4. Instalación a centro de carga para interconexión no debe estar a más de 50 metros, en caso de estarlo, el costo por metro de instalación', 5, y11);
      y11 += 3;
      doc.text('de inversor a centro de carga de interconexión tendrá un costo adicional, el costo estimado es de $6,000 mxn mas iva por metro adicional.', 8, y11);
      y11 += 7;
      doc.text('5. Sistema de monitoreo de inversores.', 5, y11);
      y11 += 7;
      doc.text('6. Medidor bidireccional.', 5, y11);
      y11 += 7;
      doc.setFontType('bold');
      doc.text('NO INCLUYE:', 5, y11);
      y11 += 7;
      doc.setFontType('normal');
      doc.text('1. Malla perimetral, obra civil.', 5, y11);
      y11 += 7;
      doc.text('2. Cliente deberá dejar tierra o planicie de area a instalar, así como cuarto de material para inversores. Se recomienda cuarto de inversores', 5, y11);
      y11 += 3;
      doc.text('para mayor seguridad, aunque estos inversores pueden ir a intemperie en este modelo.', 8, y11);
      y11 += 7;
      doc.text('3. Centro de carga para interconexión ya sea acercar la línea o centro a no más de 50 metros de cuarto inversores o', 5, y11);
      y11 += 3;
      doc.text('pago adicional por metro para la interconexión.', 8, y11);
      y11 += 7;
      doc.text('4. Uvie, el cliente debe contar con ella vigente, CFE la requerirá.', 5, y11);
      y11 += 7;
      doc.addImage(footer, 5, y11, 200, 49.47);

      if (email) {
        const pdf = doc.output('datauristring').split(',')[1];
        Meteor.call('enviar.pdf', pdf, email, (error) => {
          if (!error) {
            $('.enviar-email').prop('disabled', false).html('Enviar email');
            Alert.success('Cotización enviada');
          } else {
            console.error(error);
            Alert.error('Algo salio mal');
          }
        });
      } else {
        console.log('guardando pdf');
        doc.save('cotizacion.pdf');
      }
    };
    // load images
    let loadedImages = 0;
    const checkImages = () => { if (loadedImages === 2) generatePDF(); };
    footer.onload = function () {
      loadedImages += 1;
      checkImages();
    };
    logo.onload = function () {
      loadedImages += 1;
      checkImages();
    };
    logo.crossOrigin = '';
    footer.crossOrigin = '';
    logo.src = '/img/logo-2.png';
    footer.src = '/img/pdf-footer.png';
  },
};
export { PDF as default };
