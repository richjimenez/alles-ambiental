import iziToast from 'izitoast';

const Alert = {
  success: (message) => {
    iziToast.success({
      message: message,
      position: 'topRight',
      timeout: 4000,
      transitionIn: 'bounceInLeft',
    });
  },
  warning: (message) => {
    iziToast.warning({
      message: message,
      position: 'topRight',
      timeout: 4000,
      transitionIn: 'bounceInLeft',
    });
  },
  error: (message) => {
    iziToast.error({
      message: message,
      position: 'topRight',
      timeout: 4000,
      transitionIn: 'bounceInLeft',
    });
  },
  delete: (method, argsObject, succesMessage, callback) => {
    iziToast.warning({
      timeout: false,
      close: false,
      overlay: true,
      displayMode: 'once',
      zindex: 999,
      title: 'Are you sure?',
      message: 'You won\'t be able to revert this!',
      position: 'center',
      buttons: [
        ['<button><b>YES</b></button>', function (instance, toast) {
          const selector = $(toast);
          selector.find('.iziToast-title').html('<i class="fas fa-circle-notch fa-spin"></i>');
          selector.find('.iziToast-message').html('Please wait');
          selector.find('.iziToast-buttons').hide();
          Meteor.call(method, argsObject, (error) => {
            if (!error) {
              instance.hide({ transitionOut: 'fadeOut' }, toast, 'yes');
              iziToast.success({
                title: 'Done',
                position: 'topRight',
                message: succesMessage,
              });
              callback(null, true);
            }else{
              callback(error, null);
            }
          });
        }, true],
        ['<button>NO</button>', function (instance, toast) {
          instance.hide({ transitionOut: 'fadeOut' }, toast, 'no');
        }],
      ],
    });
  },
};

export { Alert };
