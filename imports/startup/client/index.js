// Import client startup through a single index entry point
import "./routes.js";
import log from "loglevel";
import { Meteor } from "meteor/meteor";

Meteor.startup(() => {
  log.enableAll();
  // init google maps api
  GoogleMaps.load({
    key: "AIzaSyAeoJ5nuKXULiNlmdlShlw7b4Gi0f0kYB8",
    libraries: "places"
  });
});
