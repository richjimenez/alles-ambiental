import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import admin from './admin';
// Pages
import '/imports/ui/pages/admin/projects/projects';
import '/imports/ui/pages/admin/projects/add';
import '/imports/ui/pages/admin/projects/edit';

const projects = admin.group({
  prefix: '/projects',
  name: 'admin.projects',
});

projects.route('/list/:id', {
  name: 'admin.projects.list',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminProjects' });
  },
});

projects.route('/add', {
  name: 'admin.projects.add',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminAddProject' });
  },
});

projects.route('/edit/:id', {
  name: 'admin.projects.edit',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminEditProject' });
  },
});

