import '/imports/ui/pages/admin/dashboard/dashboard';

const admin = FlowRouter.group({
  prefix: '/admin',
  name: 'admin',
  triggersEnter: [(context, redirect) => {
    if (!Meteor.userId()) redirect('/login');
  }],
});

admin.route('/', {
  name: 'admin.dashboard',
  // action() {
  //   BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminDashboard' });
  // },
  triggersEnter: [(context, redirect) => {
    redirect('/admin/projects/list/all');
  }],
});

export default admin;
