import admin from "./admin";

// Pages
import "/imports/ui/pages/admin/users/users";
import "/imports/ui/pages/admin/users/add";
import "/imports/ui/pages/admin/users/edit";
import "/imports/ui/pages/admin/users/enroll";

const users = admin.group({
  prefix: "/users",
  name: "admin.users"
});

users.route("/list/:id", {
  name: "admin.users.list",
  action() {
    BlazeLayout.render("adminBody", {
      sidebar: "adminSidebar",
      header: "adminHeader",
      main: "adminUsers"
    });
  }
});

users.route("/add", {
  name: "admin.users.add",
  action() {
    BlazeLayout.render("adminBody", {
      sidebar: "adminSidebar",
      header: "adminHeader",
      main: "adminAddUser"
    });
  }
});

users.route("/edit/:id", {
  name: "admin.users.edit",
  action() {
    BlazeLayout.render("adminBody", {
      sidebar: "adminSidebar",
      header: "adminHeader",
      main: "adminEditUser"
    });
  }
});

FlowRouter.route("/enroll-account/:token", {
  name: "enroll",
  triggersEnter: [
    (context, redirect) => {
      if (Meteor.userId()) {
        redirect("/");
      }
    }
  ],
  action() {
    BlazeLayout.render("adminLogin", { main: "adminEnroll" });
  }
});
