import admin from './admin';

// Pages
import '/imports/ui/pages/admin/clientes/clientes';
import '/imports/ui/pages/admin/clientes/add';
import '/imports/ui/pages/admin/clientes/edit';

const clientes = admin.group({
  prefix: '/clientes',
  name: 'admin.clientes',
});

clientes.route('/list/:id', {
  name: 'admin.clientes.list',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminClientes' });
  },
});

clientes.route('/add', {
  name: 'admin.clientes.add',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminAddCliente' });
  },
});

clientes.route('/edit/:id', {
  name: 'admin.clientes.edit',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminEditCliente' });
  },
});
