import admin from './admin';
// Pages
import '/imports/ui/pages/admin/zones/zones';
import '/imports/ui/pages/admin/zones/add';
import '/imports/ui/pages/admin/zones/edit';

/* globals BlazeLayout */

const zones = admin.group({
  prefix: '/zones',
  name: 'admin.zones',
});

zones.route('/list/:id', {
  name: 'admin.zones.list',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminZones' });
  },
});

zones.route('/add', {
  name: 'admin.zones.add',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminAddZone' });
  },
});

zones.route('/edit/:id', {
  name: 'admin.zones.edit',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminEditZone' });
  },
});
