import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import admin from './admin';
// Pages
import '/imports/ui/pages/admin/catalog/catalog';
import '/imports/ui/pages/admin/catalog/add';
import '/imports/ui/pages/admin/catalog/edit';

const catalog = admin.group({
  prefix: '/catalog',
  name: 'admin.catalog',
});

catalog.route('/list/:id', {
  name: 'admin.catalog.list',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminCatalog' });
  },
});

catalog.route('/add', {
  name: 'admin.catalog.add',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminAddProduct' });
  },
});

catalog.route('/edit/:id', {
  name: 'admin.catalog.edit',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminEditProduct' });
  },
});