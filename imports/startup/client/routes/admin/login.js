import '/imports/ui/pages/admin/users/login';

FlowRouter.route('/login', {
  name: 'login',
  triggersEnter: [(context, redirect) => {
    if (Meteor.userId()) {
      redirect('/admin');
    }
  }],
  action() {
    BlazeLayout.render('adminLogin', { main: 'login' });
  },
});
