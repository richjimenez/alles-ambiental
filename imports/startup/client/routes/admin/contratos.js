import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import admin from './admin';
import '/imports/ui/pages/admin/contratos/fisico';

const contratos = admin.group({
  prefix: '/contratos',
  name: 'admin.contratos',
});

contratos.route('/fisico/:id', {
  name: 'admin.contratos.fisico',
  action() {
    BlazeLayout.render('contratos', { main: 'contratoFisico' });
  },
});

contratos.route('/moral/:id', {
  name: 'admin.contratos.moral',
  action() {
    BlazeLayout.render('contratos', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminCatalog' });
  },
});
