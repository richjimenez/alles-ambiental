import admin from './admin';
// Pages
import '/imports/ui/pages/admin/reports/reports';
// import '/imports/ui/pages/admin/zones/add';
// import '/imports/ui/pages/admin/zones/edit';

/* globals BlazeLayout */

const reports = admin.group({
  prefix: '/reports',
  name: 'admin.reports',
});

reports.route('/list/:id', {
  name: 'admin.reports.list',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminReports' });
  },
});

reports.route('/add', {
  name: 'admin.reports.add',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminAddZone' });
  },
});

reports.route('/edit/:id', {
  name: 'admin.reports.edit',
  action() {
    BlazeLayout.render('adminBody', { sidebar: 'adminSidebar', header: 'adminHeader', main: 'adminEditZone' });
  },
});
