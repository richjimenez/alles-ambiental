import { FlowRouter } from "meteor/kadira:flow-router";
import { BlazeLayout } from "meteor/kadira:blaze-layout";
import admin from "./admin";
// Pages
import "/imports/ui/pages/admin/settings/settings";

const settings = admin.group({
  prefix: "/settings",
  name: "admin.settings"
});

settings.route("/", {
  name: "admin.settings.main",
  action() {
    BlazeLayout.render("adminBody", {
      sidebar: "adminSidebar",
      header: "adminHeader",
      main: "adminSettings"
    });
  }
});
