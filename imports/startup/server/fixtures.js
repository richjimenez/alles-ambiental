// Fill the DB with example data on startup

import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { Users } from '../../api/users/users';
import { Settings } from '../../api/settings/settings';
import { Products } from '../../api/catalog/products';
import { Counters } from '/imports/api/counters/counters';

Meteor.startup(() => {
  // Create a Super Admin user if no users on DB
  if (Users.find().count() === 0) {
    Accounts.createUser({
      username: 'admin',
      email: 'admin@allesambiental.com',
      password: 'alles2018',
      profile: {
        nombre: 'Alles',
        apellidos: 'Admin',
        rol: 'sadmin',
      },
    });
  }

  // init settings
  if (Settings.find().count() === 0) {
    console.log('Init Settings');
    Settings.insert({
      _id: 'tarifas', dac: 3.6, pbdt: 4.1, gdmto: 4.0, gdmth: 4.0,
    });
    Settings.insert({
      _id: 'horas',
      meses: [184.45, 186.76, 225.99, 210.9, 203.67, 185.4, 172.98, 165.23, 158, 183.83, 189.3, 172.98],
    });
  }

  // Init Seq's
  if (Counters.find().count() === 0) {
    console.log('Init Sequences');
    Counters.insert({
      _id: 'folio',
      seq: 0,
    });
  }

  // init panels
  if (Products.find().count() === 0) {
    console.log('Init Panels');
    Products.insert({
      tipo: 'Panel',
      nombre: 'POLICRISTALINO RISEN ENERGY',
      modelo: 'RSM726-330P',
      watts: 330,
    });
    Products.insert({
      tipo: 'Panel',
      nombre: 'POLICRISTALINO RISEN ENERGY',
      modelo: 'RSM72-6-370M',
      watts: 370,
    });
    Products.insert({
      tipo: 'Panel',
      nombre: 'POLICRISTALINO RISEN ENERGY',
      modelo: 'RSM60-6-275P',
      watts: 275,
    });
  }

  // Configures "reset password account" email link
  Accounts.urls.resetPassword = function(token) {
    return Meteor.absoluteUrl(`reset-password/${token}`);
  };

  // Configures "enroll account" email link
  Accounts.urls.enrollAccount = function(token) {
    return Meteor.absoluteUrl(`enroll-account/${token}`);
  };

  // Configures "verify email" email link
  Accounts.urls.verifyEmail = function(token) {
    return Meteor.absoluteUrl(`verify-email/${token}`);
  };
});

