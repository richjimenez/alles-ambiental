import { Accounts } from 'meteor/accounts-base';

Accounts.emailTemplates.siteName = 'Alles Solar';
Accounts.emailTemplates.from = 'Alles Solar <noreply@allesambiental.com>';

Accounts.emailTemplates.enrollAccount.subject = (user) => {
  return `Bienvenido, ${user.profile.nombre}`;
};

Accounts.emailTemplates.enrollAccount.text = (user, url) => {
  return `Para activar tu cuenta da click en el siguiente enlace :\n\n ${url}`;
};

Accounts.emailTemplates.resetPassword.from = () => {
  // Overrides the value set in `Accounts.emailTemplates.from` when resetting
  // passwords.
  return 'AwesomeSite Password Reset <no-reply@example.com>';
};
Accounts.emailTemplates.verifyEmail = {
  subject() {
    return 'Activate your account now!';
  },
  text(user, url) {
    return `Hey ${user}! Verify your e-mail by following this link: ${url}`;
  },
};