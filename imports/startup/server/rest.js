if (Meteor.isServer) {

  // Global API configuration
  var Api = new Restivus({
    useDefaultAuth: true,
    prettyJson: true
  });

  Api.addRoute('test', {}, {
    get: function () {
      return 'ok'
    }
  });
}
