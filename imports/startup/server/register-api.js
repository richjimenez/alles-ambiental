
import '../../api/users/methods';
import '../../api/users/server/publications';

import '../../api/catalog/methods';
import '../../api/catalog/publications';

import '/imports/api/projects/methods';
import '/imports/api/projects/publications';

import '/imports/api/settings/methods';
import '/imports/api/settings/publications';

import '/imports/api/zones/methods';
import '/imports/api/zones/publications';
