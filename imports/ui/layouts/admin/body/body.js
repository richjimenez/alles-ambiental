import './body.html';
import 'select2/dist/css/select2.min.css';
import 'select2-bootstrap4-theme/dist/select2-bootstrap4.min.css';
import 'flatpickr/dist/flatpickr.min.css';

// notifications
import iziToast from 'izitoast';
import 'izitoast/dist/css/iziToast.min.css';

import moment from 'moment';
import 'moment/locale/es-do';
import 'trumbowyg/dist/ui/trumbowyg.min.css';

// Bootstrap 4
require('bootstrap');

// Select2
require('select2')($);
require('jquery-mask-plugin');


// Flatpickr
require('flatpickr');
const Spanish = require('flatpickr/dist/l10n/es.js').default.es;

flatpickr.localize(Spanish);
moment.locale('es');

// Trumbowyg
require('trumbowyg');
require('trumbowyg/dist/plugins/noembed/trumbowyg.noembed.min');
require('trumbowyg/dist/plugins/upload/trumbowyg.upload.min');

$.trumbowyg.svgPath = '/icons/icons.svg';

//mask
require('jquery-mask-plugin');

Template.adminBody.onCreated(function() {
  $('body').removeClass().addClass('admin-body');
});
