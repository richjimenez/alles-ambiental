import './add.html';
import './add-client';

import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Users } from '/imports/api/users/users';
import { Settings } from '/imports/api/settings/settings';
import Zones from '/imports/api/zones/zones';
import { Alert } from '/imports/startup/client/alerts';
import $ from 'jquery';

const initScripts = () => {
  const voltajes = [
    {
      id: 127,
      text: '127v',
    },
    {
      id: 220,
      text: '220v',
    },
    {
      id: 440,
      text: '440v',
    },
  ];
  $('#voltaje').select2({
    theme: 'bootstrap4', placeholder: 'Selecciona un voltaje', minimumResultsForSearch: -1, data: voltajes,
  });
  const tarifa = Settings.findOne({ _id: 'tarifas' });
  const data = [
    {
      id: tarifa.dac,
      text: 'DAC',
    },
    {
      id: tarifa.pbdt,
      text: 'PBDT',
    },
    {
      id: tarifa.gdmto,
      text: 'GDMTO',
    },
    {
      id: tarifa.gdmth,
      text: 'GDMTH',
    },
  ];
  $('#tarifa-cfe').select2({
    theme: 'bootstrap4', placeholder: 'Selecciona una tarifa', minimumResultsForSearch: -1, data,
  });
  $('#cambio').val(Settings.findOne({ _id: 'tarifas' }).cambio);
  $('#watt').val(Settings.findOne({ _id: 'tarifas' }).watt);
};
const initConsumos = (fields) => {
  $('.consumos-table').show();
  const selector = $('#consumos');
  selector.empty();
  for (let i = 0; i < fields; i += 1) {
    selector.append(`
      <tr class="consumo">
        <td>
          <input type="text" class="form-control date-picker" id="from-${i}" name="from_${i}" required>
        </td>
        <td>
          <input type="text" class="form-control date-picker" id="to-${i}" name="to_${i}" required>
        </td>
        <td>
          <input type="text" class="form-control kwh" id="kwh-${i}" name="kwh_${i}" data-index="${i}" required disabled>
        </td>
        <th class="align-middle">
          <span id="total-${i}">$0</span>
        </th>
       </tr>`);
    setTimeout(() => {
      flatpickr('.date-picker', {
        altInput: true,
        altFormat: 'F j, Y',
        dateFormat: 'Z',
      });
    }, 50);
  }
};
const getTotals = () => {
  let total = 0;
  const tarifa = Session.get('tarifa');
  if (tarifa) {
    $('.consumo').each((index) => {
      const value = $(`#kwh-${index}`).val();
      if (value) {
        const m = Number((value * tarifa.valor).toFixed(2));
        $(`#total-${index}`).text(`$${m}`);
        total += m;
      } else {
        $(`#total-${index}`).text('$0');
      }
    });
    $('#total').text(`$${total.toFixed(2)}`);
  }
};
const validateConsumos = () => {
  const fields = $('.consumo').length * 2;
  let ok = 0;
  $('.consumo').each((index) => {
    const end = $(`#to-${index}`);
    const start = $(`#from-${index}`);
    if (!start.val()) {
      start.next('input').addClass('is-invalid');
    } else {
      start.next('input').removeClass('is-invalid');
      ok += 1;
    }
    if (!end.val()) {
      end.next('input').addClass('is-invalid');
    } else {
      end.next('input').removeClass('is-invalid');
      ok += 1;
    }
  });
  return ok === fields;
};
const getConsumos = () => {
  const consumos = [];
  $('.consumo').each((index) => {
    const period = $(`#period-${index}`).val();
    const kwh = Number($(`#kwh-${index}`).val());
    consumos.push({
      period, kwh,
    });
  });
  return consumos;
};
const initConsumoMensual = () => {
  $('.consumos-table').show();
  const selector = $('#consumos');
  selector.empty();
  const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  meses.forEach((mes, i) => {
    selector.append(`
      <tr class="consumo">
        <td>
          <input type="text" class="form-control" id="period-${i}" name="period-${i}" value="${mes}" readonly>
        </td>
        <td>
          <input type="text" class="form-control kwh" id="kwh-${i}" name="kwh_${i}" data-index="${i}" required disabled>
        </td>
        <th class="align-middle">
          <span id="total-${i}">$0</span>
        </th>
      </tr>
    `);
  });
};
const initConsumoBimestral = () => {
  $('.consumos-table').show();
  const selector = $('#consumos');
  selector.empty();
  const meses = ['Enero - Marzo', 'Marzo - Abril', 'Mayo - Junio', 'Julio - Agosto', 'Septiembre - Octubre', 'Noviembre - Diciembre'];
  meses.forEach((mes, i) => {
    selector.append(`
      <tr class="consumo">
        <td>
          <input type="text" class="form-control" id="period-${i}" name="period-${i}" value="${mes}" readonly>
        </td>
        <td>
          <input type="text" class="form-control kwh" id="kwh-${i}" name="kwh_${i}" data-index="${i}" required disabled>
        </td>
        <th class="align-middle">
          <span id="total-${i}">$0</span>
        </th>
      </tr>
    `);
  });
};

Template.adminAddProject.onCreated(function () {
  this.autorun(() => {
    this.subscribe('get.clients');
    this.subscribe('get.settings');
    this.subscribe('get.zones');
    if (this.subscriptionsReady()) {
      initScripts();
    }
  });
});

Template.adminAddProject.helpers({
  clientes() {
    return Users.find({ 'profile.rol': 'Cliente' });
  },
  zonas() {
    return Zones.find();
  },
});

Template.adminAddProject.events({
  'click .add-client'() {
    Modal.show('addClientModal');
  },
  'change #frecuencia'(event) {
    $('#tarifa-cfe').val('').change().prop('disabled', false);
    if (event.target.value === 'Mensual') initConsumoMensual();
    else initConsumoBimestral();
  },
  'change #empresa'(event) {
    (event.target.checked) ? $('.empresa-info').show() : $('.empresa-info').hide();
  },
  'change #tarifa-cfe'(event) {
    const result = $(event.target).select2('data')[0];
    Session.set('tarifa', { valor: Number(result.id), nombre: result.text });
    $('.kwh').prop('disabled', false);
    getTotals();
  },
  'keyup .kwh'() {
    getTotals();
  },
  'submit #add-project'(event) {
    event.preventDefault();
    const t = event.target;
    const settings = Settings.findOne({ _id: 'tarifas' });

    // if (!validateConsumos()) return Alert.error('Por favor selecciona las fechas faltantes');

    const project = {
      clienteId: t.cliente.value,
      fuente_contacto: t.fuente.value,
      estatus: t.estatus.value,
      notas: t.notas.value,
      nombre: t.nombre_proyecto.value,
      tarifa_cfe: Session.get('tarifa'),
      voltaje: Number(t.voltaje.value),
      frecuencia: t.frecuencia.value,
      numero_hilos: t.numero_hilos.value,
      numero_servicio: t.numero_servicio.value,
      zona: t.zona.value,
      watt: t.watt.value,
      cambio: t.cambio.value,
      generacion: 100,
      consumos: getConsumos(),
    };

    if (document.getElementById('empresa').checked) {
      project.empresa = {
        cargo: t.cargo.value,
        rfc: t.rfc.value,
        razon_social: t.razon_social.value,
        actividad_comercial: t.actividad_comercial.value,
      };
    }

    Meteor.call('insert.project', project, (error, result) => {
      if (!error) {
        FlowRouter.go(`/admin/projects/edit/${result}`);
      } else {
        console.log(error);
      }
    });
  },
});

Template.adminAddProject.onRendered(function () {
  $('.select2').select2({ theme: 'bootstrap4', placeholder: 'Selecciona una opcion', minimumResultsForSearch: -1 });
  $('.cliente').select2({ theme: 'bootstrap4', placeholder: 'Buscar cliente' });
  $('.empresa-info').hide();
  $('[data-toggle="tooltip"]').tooltip();
});
