import { initAutoComplete, geolocate } from '/imports/startup/client/autocomplete';
import './add-client.html';
import swal from 'sweetalert2';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

Template.addClientModal.events({
  'focus #address'() {
    geolocate();
  },
  'submit #add-client'(event) {
    event.preventDefault();
    const t = event.target;
    const data = {
      nombre: t.nombre.value,
      apellidos: t.apellidos.value,
      email: t.email.value,
      telefono: t.tel.value,
      mobile: t.mobile.value,
      rol: 'Cliente',
      direccion: {
        completa: t.address.value,
        numero: t.street_number.value,
        calle: t.street.value,
        ciudad: t.city.value,
        estado: t.state.value,
        codigo_postal: t.zip_code.value,
        pais: t.country.value,
      },
    };
    Meteor.call('insert.client', data, (error, result) => {
      if (!error) {
        swal({
          type: 'success',
          text: 'Cliente Guardado',
          heightAuto: false,
        });
        Modal.hide('addClientModal');
        $('.cliente').val(result).trigger('change');
      } else {
        console.warn(error);
        swal({
          type: 'error',
          text: error.reason,
          heightAuto: false,
        });
      }
    });
  },
});


Template.addClientModal.onRendered(function() {
  setTimeout(() => { initAutoComplete(); }, 500);
});
