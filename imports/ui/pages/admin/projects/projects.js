import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import $ from 'jquery';
import Swal from 'sweetalert2';
import './projects.html';
import './project';

// declare variables
let projects; let page; let perPage; let totalPages; let wait;
// functions
// delay the type in search input
const delay = (function() {
  let timer = 0;
  return function(callback, ms) {
    clearTimeout(timer);
    timer = setTimeout(callback, ms);
  };
}());
const loader = {
  show: (text) => {
    $('.list-loader').addClass('loader-active')
      .html(`<i class="fas fa-circle-notch fa-spin"></i><br><small>${text}</small>`);
  },
  empty: (text) => {
    $('.list-loader').addClass('loader-active')
      .html(`<i class="fas fa-ban"></i><br><small>${text}</small>`);
  },
  hide: () => {
    $('.list-loader').removeClass('loader-active');
  },
};

Template.adminProjects.onCreated(function () {
  $('html, body').animate({ scrollTop: 0 });
  // init variables values
  projects = [];
  page = 1;
  perPage = Math.floor(window.innerHeight / 30);
  totalPages = 0;
  wait = false;
  Session.set('search', null);
});

Template.adminProjects.helpers({
  projects() {
    if (Session.get('projects')) {
      return Session.get('projects');
    }
  },
  activeProject(id) {
    if (Projects.findOne()) {
      if (Projects.findOne()._id === id) {
        return 'active';
      }
    }
  },
  searchResult() {
    console.log(Session.get('search'))
    if (Session.get('search')) return Session.get('search');
    return [];
  },
});

Template.adminProjects.events({
  'click .btn-hide-list'() {
    showHideList();
  },
  'click .result'() {
    $('#search').val('');
    Session.set('search', []);
  },
  'keyup #search'(event) {
    const t = event.target;
    loader.show('Searching users');
    delay(() => {
      if (t.value.length === 0) {
        Session.set('search', null);
        loader.hide();
      } else {
        Meteor.call('search.projects', t.value, (error, response) => {
          if (!error) {
            if (response.length === 0) {
              loader.empty('No Results');
              setTimeout(() => { loader.hide(); }, 2000);
            } else {
              Session.set('search', response);
              loader.hide();
            }
          }
        });
      }
    }, 300);
  },
});


Template.adminProjects.onRendered(function () {
  window.addEventListener('resize', () => {
    const width = document.body.clientWidth;
    if (width <= 414) {
      $('.list').addClass('hide-list');
      $('.btn-hide-list').html('<i class="fas fa-list-ul"></i>');
      list = false;
    } else {
      $('.list').removeClass('hide-list');
      $('.btn-hide-list').html('<i class="fas fa-times"></i>');
      list = true;
    }
  });
  scrollEndDetector();
  getData();
});

Template.adminProjects.onDestroyed(function () {
  // clean the session variables
  delete Session.keys.projects;
  delete Session.keys.search;
});

// show & hide the list
let list = true;
let showHideList = () => {
  if (list === true) {
    $('.list').addClass('hide-list');
    $('.btn-hide-list').html('<i class="fas fa-list-ul"></i>');
    list = false;
  } else {
    $('.list').removeClass('hide-list');
    $('.btn-hide-list').html('<i class="fas fa-times"></i>');
    list = true;
  }
};

// Detect scroll ends
let scrollEndDetector = () => {
  $('.list ul').on('scroll', function () {
    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight && wait === false) {
      wait = true;
      getData();
    }
  });
};

// get data from server
let getData = () => {
  if (totalPages === 0 || page <= totalPages) {
    // Show loader
    $('.list-loader').addClass('loader-active').html('<i class="fas fa-circle-notch fa-spin"></i><br><small>Loading Projects</small>');
    // init and get the first data
    Meteor.call('get.projects.by.page', page, perPage, (error, response) => {
      if (!error) {
        response.data.forEach((element) => {
          projects.push(element);
        });
        Session.set('projects', projects);
        page++;
        totalPages = response.totalPages;
        wait = false;
        // Hide loader
        $('.list-loader').removeClass('loader-active');
      }
    });
  } else {
    $('.list-loader').addClass('loader-active').html('<i class="fas fa-ban"></i><br><small>No more records</small>');
    setTimeout(() => {
      $('.list-loader').removeClass('loader-active').html('<i class="fas fa-circle-notch fa-spin"></i><br><small>Loading Projects</small>');
      wait = false;
    }, 2000);
  }
};
