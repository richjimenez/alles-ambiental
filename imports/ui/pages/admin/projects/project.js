import './project.html';
import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { Projects } from '/imports/api/projects/projects';
import $ from 'jquery';
import moment from 'moment';

Template.adminProject.onRendered(function() {
  this.autorun(() => {
    this.subscribe('get.project', FlowRouter.getParam('id'));
    // detect path change
    FlowRouter.watchPathChange();
  });
});

Template.adminProject.helpers({
  project() {
    return Projects.findOne();
  },
  formatDate(date) {
    return moment(date).format('LLL');
  },
  total() {
    const project = Projects.findOne();
    const tarifa = project.tarifa_cfe.valor;
    let total = 0;
    project.consumos.forEach((consumo) => {
      total += consumo.kwh * tarifa;
    });
    return total.toFixed(2);
  },
  getAmount(kwh) {
    const tarifa = Projects.findOne().tarifa_cfe.valor;
    const amount = kwh * tarifa;
    return amount.toFixed(2);
  },
});

Template.adminProject.events({
  'click .edit-btn'(event) {
    FlowRouter.go(`/admin/projects/edit/${event.currentTarget.value}`);
  },
});

const getData = (id) => {
  Meteor.call('get.project', id, (error, result) => {
    if (!error) {
      Session.set('project', result);
    } else {
      console.warn(error);
    }
  });
};
