import './edit.html';
// modal agregar contrato
import '/imports/ui/pages/admin/contratos/add';
import '/imports/ui/pages/admin/contratos/edit';
import '/imports/ui/components/admin/grafica-consumo-kwh';
import '/imports/ui/components/admin/grafica-ahorro-anual';
import '/imports/ui/components/admin/amort';
import '/imports/ui/components/admin/forma-de-pago';

import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Users } from '/imports/api/users/users';
import { Settings } from '/imports/api/settings/settings';
import { Projects } from '/imports/api/projects/projects';
import { Products } from '/imports/api/catalog/products';
import { Alert } from '/imports/startup/client/alerts';
import { Session } from 'meteor/session';
import Inversores from '/imports/startup/client/inversores';
import $ from 'jquery';
import moment from 'moment';
import Dinero from 'dinero.js';
import PDF from '/imports/startup/client/pdf';
import Zones from '../../../../api/zones/zones';


moment.locale('es');

let init;


const initScripts = () => {
  if (init) return;
  init = true;
  setTimeout(() => {
    const project = Projects.findOne();
    // revisar si tiene empresa
    if (project.empresa) {
      document.getElementById('empresa').checked = true;
      $('.empresa-info').show();
    } else {
      $('.empresa-info').hide();
    }
    $('.select2').select2({ theme: 'bootstrap4', placeholder: 'Selecciona una opcion', minimumResultsForSearch: -1 });
    // init cliente select
    $('#cliente').select2({ theme: 'bootstrap4', placeholder: 'Buscar cliente' }).val(project.client.id).change();
    $('[data-toggle="tooltip"]').tooltip();
    flatpickr('.date-picker', {
      altInput: true,
      altFormat: 'F j, Y',
      dateFormat: 'Z',
    });
    $('.update-actions').hide();
    const tarifa = Settings.findOne({ _id: 'tarifas' });
    const data = [
      {
        id: tarifa.dac,
        text: 'DAC',
      },
      {
        id: tarifa.pbdt,
        text: 'PBDT',
      },
      {
        id: tarifa.gdmto,
        text: 'GDMTO',
      },
      {
        id: tarifa.gdmth,
        text: 'GDMTH',
      },
    ];
    $('#tarifa-cfe').select2({
      theme: 'bootstrap4', placeholder: 'Selecciona una tarifa', minimumResultsForSearch: -1, data,
    }).val(project.tarifa_cfe.valor).trigger('change');
    const voltajes = [
      {
        id: 127,
        text: '127v',
      },
      {
        id: 220,
        text: '220v',
      },
      {
        id: 440,
        text: '440v',
      },
    ];
    $('#voltaje').select2({
      theme: 'bootstrap4', placeholder: 'Selecciona un voltaje', minimumResultsForSearch: -1, data: voltajes,
    }).val(project.voltaje).trigger('change');
  }, 10);
};
const addConsumos = () => {
  const selector = $('#consumos');
  if ($('.consumo').length < 12) {
    for (let i = 6; i < 12; i += 1) {
      selector.append(`
      <tr class="consumo">
        <td>
          <input type="text" class="form-control date-picker" id="from-${i}" name="from_${i}" required>
        </td>
        <td>
          <input type="text" class="form-control date-picker" id="to-${i}" name="to_${i}" required>
        </td>
        <td>
          <input type="text" class="form-control kwh" id="kwh-${i}" name="kwh_${i}" data-index="${i}" required>
        </td>
        <th class="align-middle">
          <span id="total-${i}">$0</span>
        </th>
       </tr>`);
      flatpickr(`#from-${i}`, {
        altInput: true,
        altFormat: 'F j, Y',
        dateFormat: 'Z',
      });
      flatpickr(`#to-${i}`, {
        altInput: true,
        altFormat: 'F j, Y',
        dateFormat: 'Z',
      });
    }
  }
};
const validateConsumos = () => {
  const fields = $('.consumo').length * 2;
  let ok = 0;
  $('.consumo').each((index) => {
    const end = $(`#to-${index}`);
    const start = $(`#from-${index}`);
    if (!start.val()) {
      start.next('input').addClass('is-invalid');
    } else {
      start.next('input').removeClass('is-invalid');
      ok += 1;
    }
    if (!end.val()) {
      end.next('input').addClass('is-invalid');
    } else {
      end.next('input').removeClass('is-invalid');
      ok += 1;
    }
  });
  return ok === fields;
};
const getConsumos = () => {
  const consumos = [];
  $('.consumo').each((index) => {
    const period = $(`#period-${index}`).val();
    const kwh = Number($(`#kwh-${index}`).val());
    consumos.push({
      period, kwh,
    });
  });
  return consumos;
};
const getTotals = () => {
  let total = 0;
  const tarifa = Session.get('tarifa');
  if (tarifa) {
    $('.consumo').each((index) => {
      const value = Number($(`#kwh-${index}`).val());
      if (value) {
        const m = Number((value * tarifa.valor).toFixed(2));
        $(`#total-${index}`).text(`$${m}`);
        total += m;
      } else {
        $(`#total-${index}`).text('$0');
      }
    });
    $('#total').text(`$${total.toFixed(2)}`);
    Session.set('pagoActualCFE', total.toFixed(2));
  }
};
const initConsumoMensual = () => {
  const project = Projects.findOne();
  $('.consumos-table').show();
  const selector = $('#consumos');
  selector.empty();
  const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  meses.forEach((mes, i) => {
    if (project.consumos.length === 12) {
      selector.append(`
      <tr class="consumo">
        <td>
          <input type="text" class="form-control" id="period-${i}" name="period-${i}" value="${mes}" readonly>
        </td>
        <td>
          <input type="text" class="form-control kwh" id="kwh-${i}" name="kwh_${i}" data-index="${i}" value="${project.consumos[i].kwh}" required>
        </td>
        <th class="align-middle">
          <span id="total-${i}">$0</span>
        </th>
      </tr>
    `);
    } else {
      selector.append(`
      <tr class="consumo">
        <td>
          <input type="text" class="form-control" id="period-${i}" name="period-${i}" value="${mes}" readonly>
        </td>
        <td>
          <input type="text" class="form-control kwh" id="kwh-${i}" name="kwh_${i}" data-index="${i}" required>
        </td>
        <th class="align-middle">
          <span id="total-${i}">$0</span>
        </th>
      </tr>
    `);
    }
  });
};
const initConsumoBimestral = () => {
  const project = Projects.findOne();
  $('.consumos-table').show();
  const selector = $('#consumos');
  selector.empty();
  const meses = ['Enero - Marzo', 'Marzo - Abril', 'Mayo - Junio', 'Julio - Agosto', 'Septiembre - Octubre', 'Noviembre - Diciembre'];
  meses.forEach((mes, i) => {
    if (project.consumos.length === 6) {
      selector.append(`
      <tr class="consumo">
        <td>
          <input type="text" class="form-control" id="period-${i}" name="period-${i}" value="${mes}" readonly>
        </td>
        <td>
          <input type="text" class="form-control kwh" id="kwh-${i}" name="kwh_${i}" data-index="${i}" value="${project.consumos[i].kwh}" required>
        </td>
        <th class="align-middle">
          <span id="total-${i}">$0</span>
        </th>
      </tr>
    `);
    } else {
      selector.append(`
      <tr class="consumo">
        <td>
          <input type="text" class="form-control" id="period-${i}" name="period-${i}" value="${mes}" readonly>
        </td>
        <td>
          <input type="text" class="form-control kwh" id="kwh-${i}" name="kwh_${i}" data-index="${i}" required>
        </td>
        <th class="align-middle">
          <span id="total-${i}">$0</span>
        </th>
      </tr>
    `);
    }
  });
};
const openPopUp = () => {
  const w = 900;
  const h = screen.height - 100;
  const left = (screen.width / 2) - (w / 2);
  const top = (screen.height / 2) - (h / 2);
  const url = `${Meteor.absoluteUrl()}admin/contratos/fisico/${FlowRouter.getParam('id')}`;
  const newWindow = window.open(url, 'Contrato', `toolbar=no, location=no, directories=no, status=no, 
  menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=${w}, height=${h}, top=${top}, left=${left}`);
  if (window.focus) newWindow.focus();
};

Template.adminEditProject.onCreated(function () {
  init = false;
  this.subscribe('get.project', FlowRouter.getParam('id'));
  this.subscribe('get.clients');
  this.subscribe('get.settings');
  this.subscribe('get.products');
  this.subscribe('get.zones');
  this.autorun(() => {
    if (this.subscriptionsReady()) {
      Session.set('tarifa', Projects.findOne().tarifa_cfe);
      const project = Projects.findOne();
      const horas = Settings.findOne({ _id: 'horas' });
      let totalHoras = 0;
      horas.meses.forEach((value) => {
        totalHoras += value;
      });
      Session.set('totalHoras', totalHoras);
      if (project.panel) Session.set('potencia', project.panel.watts);
      initScripts();
      setTimeout(() => {
        getTotals();
      }, 50);
    }
  });
});
Template.adminEditProject.helpers({
  project() {
    return Projects.findOne();
  },
  clientes() {
    return Users.find({ 'profile.rol': 'Cliente' });
  },
  formatDate(date) {
    return moment(date).format('DD / MM / YYYY');
  },
  paneles() {
    return Products.find({ tipo: 'Panel' });
  },
  // maths
  potencia() {
    return Session.get('potencia');
  },
  userHasAccess() {
    const user = Meteor.user();
    return user.profile.rol === 'sadmin' || user.profile.rol === 'admin';
  },
  pagoActualCFE() {
    if (Session.get('pagoActualCFE')) {
      const result = Session.get('pagoActualCFE').split('.').join('');
      return Dinero({ amount: Number(result) }).toFormat('$0,0.00');
    }
    return [];
  },
  zonas() {
    return Zones.find();
  },
  porcentajePagoActualCFE() {
    if (Session.get('pagoActualCFE')) {
      Session.set('pGrafica1', (Number(Session.get('pagoActualCFE')) / 300000 * 100).toFixed(1));
      return Session.get('pGrafica1');
    }
    return [];
  },
  pagoRealCFE() {
    let total = 0;
    const totalXmes = [];
    const project = Projects.findOne();
    const horas = Settings.findOne({ _id: 'horas' });
    let mesAnterior = 0;
    if (Session.get('energiaMensual') && Session.get('sistemaFotovoltaico')) {
      project.consumos.forEach((consumo, index) => {
        const pagoActual = (Session.get('energiaMensual') * project.tarifa_cfe.valor).toFixed(2);
        const ahorroPromedio = (horas.meses[index] * Session.get('sistemaFotovoltaico') * 0.73 * project.tarifa_cfe.valor).toFixed(2);
        const result = pagoActual - ahorroPromedio;
        if (index > 0) {
          if (result < 0) {
            const suma = result + mesAnterior;
            if (suma > 0) totalXmes.push(suma);
            else totalXmes.push(0);
          } else {
            totalXmes.push(result);
          }
        } else {
          totalXmes.push(result);
        }
        mesAnterior = result;
      });
    }
    totalXmes.forEach((element) => {
      total += element;
    });
    Session.set('pagoRealNoFormat', total);
    total = total.toFixed(2).toString().split('.').join('');
    Session.set('pagoReal', Dinero({ amount: Number(total) }).toFormat('$0,0.00'));
    return Dinero({ amount: Number(total) }).toFormat('$0,0.00');
  },
  porcentajePagoRealCFE() {
    console.log(Session.get('pagoRealNoFormat'));
    Session.set('pGrafica2', (Number(Session.get('pagoRealNoFormat')) / 300000 * 100).toFixed(1));
    return Session.get('pGrafica2');
  },
  consumoActualMensual() {
    const project = Projects.findOne();
    let totalCosumos = 0;
    project.consumos.forEach((consumo) => {
      totalCosumos += consumo.kwh;
    });
    Session.set('consumoActual', Math.round(totalCosumos / 12));
    return Session.get('consumoActual');
  },
  numeroDePaneles() {
    const project = Projects.findOne();
    const factorRegeneracion = Session.get('totalHoras') * 73;
    const r1 = Session.get('consumoActual') * 12 / factorRegeneracion;
    const r2 = Session.get('potencia') / 1000;
    Session.set('numeroDePaneles', Math.round(r1 / r2 * project.generacion));
    return Session.get('numeroDePaneles');
  },
  sistemaFotovoltaico() {
    const result = Session.get('numeroDePaneles') * Session.get('potencia') / 1000;
    Session.set('sistemaFotovoltaico', result);
    return result;
  },
  superficie() {
    const superficie = (Session.get('numeroDePaneles') * 2 * 1.3).toFixed(2);
    Session.set('superficie', superficie);
    return superficie;
  },
  generacionEnergiaMensual() {
    Session.get('potencia');
    Session.get('sistemaFotovoltaico');
    const horas = Settings.findOne({ _id: 'horas' });
    let total = 0;
    horas.meses.forEach((value) => {
      total += value * Session.get('sistemaFotovoltaico') * 0.73 / 12;
    });
    Session.set('energiaMensual', Math.round(total));
    return Session.get('energiaMensual');
  },
  ahorroAnual() {
    let result = (Session.get('energiaMensual') * Session.get('tarifa').valor * 12).toFixed(2);
    result = result.split('.').join('');
    Session.set('ahorroAnual', Dinero({ amount: Number(result) }).toFormat('$0,0.00'));
    return Dinero({ amount: Number(result) }).toFormat('$0,0.00');
  },
  porcentajeAhorroAnual() {
    const ahorroAnual = (Session.get('energiaMensual') * Session.get('tarifa').valor * 12).toFixed(2);
    Session.set('pGrafica3', (Number(ahorroAnual) / 300000 * 100).toFixed(1));
    return Session.get('pGrafica3');
  },
  porcentajeAhorro() {
    return (Session.get('energiaMensual') / Session.get('consumoActual') * 100).toFixed(2);
  },
  subtotal() {
    if (Session.get('sistemaFotovoltaico')) {
      const project = Projects.findOne();
      let result = (Session.get('sistemaFotovoltaico') * 1000 * project.watt * project.cambio);
      result = result.toFixed(2).toString().split('.').join('');
      Session.set('subTotal', Dinero({ amount: Number(result) }).toFormat('$0,0.00'));
      return Dinero({ amount: Number(result) }).toFormat('$0,0.00');
    }
  },
  iva() {
    const project = Projects.findOne();
    const total = Session.get('sistemaFotovoltaico') * 1000 * project.watt * project.cambio;
    let result = 0.16 * total;
    result = result.toFixed(2).toString().split('.').join('');
    Session.set('iva', Dinero({ amount: Number(result) }).toFormat('$0,0.00'));
    return Dinero({ amount: Number(result) }).toFormat('$0,0.00');
  },
  total() {
    const project = Projects.findOne();
    const total = Session.get('sistemaFotovoltaico') * 1000 * project.watt * project.cambio;
    const iva = 0.16 * total;
    let result = total + iva;
    Session.set('totalProjecto', result);
    result = result.toFixed(2).toString().split('.').join('');
    Session.set('granTotal', Dinero({ amount: Number(result) }).toFormat('$0,0.00'));
    return Dinero({ amount: Number(result) }).toFormat('$0,0.00');
  },
});
Template.adminEditProject.events({
  'change #tarifa-cfe'(event) {
    const result = $(event.target).select2('data')[0];
    Session.set('tarifa', { valor: Number(result.id), nombre: result.text });
    $('.kwh').prop('disabled', false);
    getTotals();
  },
  'change #voltaje'(event) {
    const value = Number(event.target.value);
    const selector = $('#potencia-panel');
    if (value === 127) {
      const data = [
        {
          id: 270,
          text: 'Panel 270w',
        },
      ];
      selector.empty().trigger('change');
      selector.select2({
        theme: 'bootstrap4', placeholder: 'Tipo de panel', minimumResultsForSearch: -1, data,
      });
    } else {
      const data = [
        {
          id: 330,
          text: 'Panel 330w',
        },
        {
          id: 345,
          text: 'Panel 345w',
        },
        {
          id: 370,
          text: 'Panel 370w',
        },
      ];
      selector.empty().trigger('change');
      selector.select2({
        theme: 'bootstrap4', placeholder: 'Tipo de panel', minimumResultsForSearch: -1, data,
      });
    }
  },
  'change #empresa'(event) {
    if (event.target.checked) {
      $('.empresa-info').show();
      $('#cargo, #rfc, #razon, #acttividad').prop('required', true);
    } else {
      $('.empresa-info').hide();
      $('#cargo, #rfc, #razon, #acttividad').prop('required', false);
    }
  },
  'change #frecuencia'(event) {
    if (event.target.value === 'Mensual') initConsumoMensual();
    else initConsumoBimestral();
    getTotals();
  },
  'keyup .kwh'() {
    getTotals();
  },
  'submit #update-info'(event) {
    event.preventDefault();
    const t = event.target;
    const data = {
      id: FlowRouter.getParam('id'),
      clienteId: t.cliente.value,
      fuente_contacto: t.fuente.value,
      estatus: t.estatus.value,
      notas: t.notas.value,
      nombre: t.nombre_proyecto.value,
    };
    if (document.getElementById('empresa').checked) {
      data.empresa = {
        cargo: t.cargo.value,
        rfc: t.rfc.value,
        razon_social: t.razon_social.value,
        actividad_comercial: t.actividad_comercial.value,
      };
    } else {
      data.empresa = null;
    }
    Meteor.call('update.project', data, (error) => {
      if (!error) Alert.success('Informacion actualizada');
    });
  },
  'submit #update-consumo'(event) {
    event.preventDefault();
    const form = event.target;
    const data = {
      id: FlowRouter.getParam('id'),
      tarifa_cfe: Session.get('tarifa'),
      voltaje: Number(form.voltaje.value),
      frecuencia: form.frecuencia.value,
      numero_hilos: form.numero_hilos.value,
      numero_servicio: form.numero_servicio.value,
      zona: form.zona.value,
      watt: Number(form.watt.value),
      cambio: Number(form.cambio.value),
      consumos: getConsumos(),
      generacion: Number(form.generacion.value),
    };
    Meteor.call('update.consumo', data, (error) => {
      if (!error) {
        Alert.success('Información de consumo actualizada');
        window.location.reload();
      }
    });
  },
  'change #panel'(event) {
    const project = Projects.findOne();
    const val = event.target.value.split('|');
    // insert panel
    const panel = {
      id: val[0],
      nombre: val[1],
      watts: val[2],
    };

    Meteor.call('insert.panel', FlowRouter.getParam('id'), panel, (error) => {
      if (!error) {
        setTimeout(() => {
          const inv = Inversores.get('contado', Session.get('numeroDePaneles'), Number(val[2]), project.voltaje);
          if (inv.length === 1 && inv[0].cantidad === 0) {
            Meteor.call('remove.inversores', FlowRouter.getParam('id'), (err) => {
              if (!err) Alert.error(inv[0].nombre);
            });
          } else {
            Meteor.call('insert.inversores', FlowRouter.getParam('id'), inv, (err) => {
              if (!err) Alert.success('Inversores guardados');
              else Alert.error('Algo salio mal');
            });
          }
        }, 100);
      }
    });


    // const inv = Inversores.get('contado', Session.get('numeroDePaneles'), Number(panel[2]), project.voltaje);
    //
    // if (inv.length === 1 && inv[0].cantidad === 0) throw Alert.error(inv[0].inversor);
    //
    // const result = {
    //   id: FlowRouter.getParam('id'),
    //   panel: {
    //     id: panel[0],
    //     nombre: panel[1],
    //     watts: panel[2],
    //   },
    //   inversores: inv,
    // };
    //
    // Meteor.call('add.panel.and.inversor', result, (error) => {
    //   if (!error) console.log('done');
    // });
  },
  'click .set-panel'() {
    // const panel = $('#panel');
    // if (!panel.val()) throw Alert.warning('Selecciona un panel');
    // const data = {
    //   id: FlowRouter.getParam('id'),
    //   panelId: panel.val().split('|')[0],
    //   nombre: panel.val().split('|')[1],
    //   watts: panel.val().split('|')[2],
    // };
    // Meteor.call('set.panel', data, (error) => {
    //   if (!error) Alert.success('Panel seleccionado');
    //   else Alert.error('Algo salio mal');
    // });
  },
  'click .generar-contrato'() {
    Modal.show('addContrato');
  },
  'click .ver-contrato'() {
    openPopUp();
  },
  'click .editar-contrato'() {
    Modal.show('editContrato');
  },
  'click .generar-pdf'() {
    const project = Projects.findOne();
    // validations
    if (!project.panel) throw Alert.error('Selecciona un panel');
    if (!project.inversores) throw Alert.error('Genera inversores validos');
    if (!project.pago) throw Alert.error('Selecciona metodo de pago');
    const data = {
      folio: project.folio.toString(),
      fecha: moment(project.createdAt).format('DD / MM / YYYY'),
      pago: project.pago,
      cambio: project.cambio.toString(),
      cliente: {
        nombre: project.client.nombre,
        apellidos: project.client.apellidos,
      },
      zona: project.zona,
      numero_servicio: project.numero_servicio,
      tarifa: project.tarifa_cfe,
      generacionMensual: Session.get('energiaMensual').toString(),
      consumoActualMensual: Session.get('consumoActual').toString(),
      ahorroMensual: `${(Session.get('energiaMensual') / Session.get('consumoActual') * 100).toFixed(2)}%`,
      pagoActual: Dinero({ amount: Number(Session.get('pagoActualCFE').split('.').join('')) }).toFormat('$0,0.00'),
      pagoReal: Session.get('pagoReal'),
      ahorroAnual: Session.get('ahorroAnual'),
      pGrafica1: Session.get('pGrafica1'),
      pGrafica2: Session.get('pGrafica2'),
      pGrafica3: Session.get('pGrafica3'),
      voltaje: project.voltaje.toString(),
      sistemaFoto: `${Session.get('sistemaFotovoltaico')} KWP instalados`,
      superficie: `${Session.get('superficie')} m2`,
      noPaneles: Session.get('numeroDePaneles').toString(),
      panel: `${project.panel.watts} WP, ${project.panel.nombre}`,
      inversores: project.inversores,
      subTotal: Session.get('subTotal'),
      iva: Session.get('iva'),
      total: Session.get('granTotal'),
    };
    PDF.generate(data);
  },
  'click .enviar-email'() {
    $('.enviar-email').prop('disabled', true).html('Enviando... <i class="fas fa-spin fa-spinner"></i>');
    const project = Projects.findOne();
    // validations
    if (!project.panel) throw Alert.error('Selecciona un panel');
    if (!project.inversores) throw Alert.error('Genera inversores validos');
    if (!project.pago) throw Alert.error('Selecciona método de pago');
    const data = {
      folio: project.folio.toString(),
      fecha: moment(project.createdAt).format('DD / MM / YYYY'),
      pago: project.pago,
      cambio: project.cambio.toString(),
      cliente: {
        nombre: project.client.nombre,
        apellidos: project.client.apellidos,
      },
      zona: project.zona,
      numero_servicio: project.numero_servicio,
      tarifa: project.tarifa_cfe,
      generacionMensual: Session.get('energiaMensual').toString(),
      consumoActualMensual: Session.get('consumoActual').toString(),
      ahorroMensual: `${(Session.get('energiaMensual') / Session.get('consumoActual') * 100).toFixed(2)}%`,
      pagoActual: Dinero({ amount: Number(Session.get('pagoActualCFE').split('.').join('')) }).toFormat('$0,0.00'),
      pagoReal: Session.get('pagoReal'),
      ahorroAnual: Session.get('ahorroAnual'),
      pGrafica1: Session.get('pGrafica1'),
      pGrafica2: Session.get('pGrafica2'),
      pGrafica3: Session.get('pGrafica3'),
      voltaje: project.voltaje.toString(),
      sistemaFoto: `${Session.get('sistemaFotovoltaico')} KWP instalados`,
      superficie: `${Session.get('superficie')} m2`,
      noPaneles: Session.get('numeroDePaneles').toString(),
      panel: `${project.panel.watts} WP, ${project.panel.nombre}`,
      inversores: project.inversores,
      subTotal: Session.get('subTotal'),
      iva: Session.get('iva'),
      total: Session.get('granTotal'),
    };
    PDF.generate(data, project.client.email);
  },
});
Template.adminEditProject.onRendered(function () {
});
