import './edit.html';

import { Meteor } from 'meteor/meteor';
import { Users } from '/imports/api/users/users';
import { initAutoComplete, geolocate } from '/imports/startup/client/autocomplete';
import swal from 'sweetalert2';

Template.adminEditCliente.onCreated(function () {
  this.autorun(() => {
    this.subscribe('get.user', FlowRouter.getParam('id'));
  });
});

Template.adminEditCliente.helpers({
  user() {
    return Users.findOne({ _id: FlowRouter.getParam('id') });
  },
});

Template.adminEditCliente.events({
  'focus #address'() {
    geolocate();
  },
  'submit #update-user-info'(event) {
    event.preventDefault();
    const id = FlowRouter.getParam('id');
    const t = event.target;
    const profile = {
      nombre: t.nombre.value,
      apellidos: t.apellidos.value,
      telefono: t.tel.value,
      mobile: t.mobile.value,
      rol: 'cliente',
      direccion: {
        completa: t.address.value,
        numero: t.street_number.value,
        calle: t.street.value,
        ciudad: t.city.value,
        estado: t.state.value,
        codigo_postal: t.zip_code.value,
        pais: t.country.value,
      },
    };
    Meteor.call('update.client.info', id, profile, (error) => {
      if (!error) {
        swal({
          type: 'success',
          text: 'Cliente actualizado',
          heightAuto: false,
        }).then(() => {
          FlowRouter.go(`/admin/clientes/list/${id}`);
        });
      }
    });
  },
  'click .update-username'() {
    const id = FlowRouter.getParam('id');
    const username = $('#username').val();
    Meteor.call('update.username', id, username, (error) => {
      if (error) {
        console.log(error);
      } else {
        console.log('changed');
      }
    });
  },
});

Template.adminEditCliente.onRendered(function() {
  setTimeout(() => {
    initAutoComplete();
    $('.select2').select2({ theme: 'bootstrap4', placeholder: 'Select one option', minimumResultsForSearch: -1 });
  }, 100);
});
