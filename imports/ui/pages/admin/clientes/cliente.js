import './cliente.html';
import { Users } from '/imports/api/users/users';

Template.adminCliente.onCreated(function () {
  this.autorun(() => {
    // detect path change
    FlowRouter.watchPathChange();
    this.subscribe('get.user', FlowRouter.getParam('id'));
  });
});

Template.adminCliente.helpers({
  user() {
    if (FlowRouter.getParam('id') === 'all') {
      return Users.findOne();
    }
    return Users.findOne({ _id: FlowRouter.getParam('id') });
  },
});

Template.adminCliente.events({
  'click .edit-btn'(e) {
    FlowRouter.go(`/admin/clientes/edit/${$(e.target).data('id')}`);
  },
});
