import './add.html';
import { Meteor } from 'meteor/meteor';
import { initAutoComplete, geolocate } from '/imports/startup/client/autocomplete';
import { Alert } from '/imports/startup/client/alerts';
import swal from 'sweetalert2';

Template.adminAddCliente.events({
  'focus #address'() {
    geolocate();
  },
  'submit #add-cliente'(event) {
    event.preventDefault();
    $('input').prop('disabled', true);
    $('select').prop('disabled', true);
    $("button[type='submit']").prop('disabled', true).html('Guardando... <i class="fas fa-spin fa-spinner"></i>');
    const t = event.target;
    const user = {
      nombre: t.nombre.value,
      apellidos: t.apellidos.value,
      email: t.email.value,
      telefono: t.tel.value,
      mobile: t.mobile.value,
      rol: 'Cliente',
      direccion: {
        completa: t.address.value,
        numero: t.street_number.value,
        calle: t.street.value,
        ciudad: t.city.value,
        estado: t.state.value,
        codigo_postal: t.zip_code.value,
        pais: t.country.value,
      },
    };
    Meteor.call('insert.client', user, (error, result) => {
      if (!error) {
        swal({
          type: 'success',
          text: 'Cliente Guardado',
          heightAuto: false,
        }).then(() => {
          FlowRouter.go(`/admin/clientes/list/${result}`);
        });
      } else {
        console.error(error);
        Alert.error(error.reason);
        $('input').prop('disabled', false);
        $('select').prop('disabled', false);
        $("button[type='submit']").prop('disabled', false).html('Guardar cliente');
      }
    });
  },
});

Template.adminAddCliente.onRendered(function() {
  setTimeout(() => { initAutoComplete(); }, 500);
  $('.select2').select2({ theme: 'bootstrap4', placeholder: 'Select one option', minimumResultsForSearch: -1 });
});
