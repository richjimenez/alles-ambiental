import './clientes.html';
import './cliente';

import { Session } from 'meteor/session';


// declare variables
let users; let page; let perPage; let totalPages; let wait;

Template.adminClientes.onCreated(function () {
  // init variables values
  users = [];
  page = 1;
  perPage = Math.floor(window.innerHeight / 50);
  totalPages = 0;
  wait = false;
});

Template.adminClientes.helpers({
  users() {
    if (Session.get('users')) return Session.get('users');
    return [];
  },
  activeUser(id) {
    if (id === FlowRouter.getParam('id')) {
      return 'active';
    }
  },
});

Template.adminClientes.events({
  'click #logout'(e) {
    e.preventDefault();
    Meteor.logout(() => {
      location.reload();
    });
  },
  'click .btn-hide-list'() {
    showHideList();
  },
});

Template.adminClientes.onRendered(function () {
  window.addEventListener('resize', () => {
    const width = document.body.clientWidth;
    if (width <= 414) {
      $('.list').addClass('hide-list');
      $('.btn-hide-list').html('<i class="fas fa-list-ul"></i>');
      list = false;
    } else {
      $('.list').removeClass('hide-list');
      $('.btn-hide-list').html('<i class="fas fa-times"></i>');
      list = true;
    }
  });
  scrollEndDetector();
  getData();
});

Template.adminClientes.onDestroyed(function () {
  // clean the session variables
  Session.keys = {};
});

// show & hide the list
let list = true;
let showHideList = () => {
  if (list === true) {
    $('.list').addClass('hide-list');
    $('.btn-hide-list').html('<i class="fas fa-list-ul"></i>');
    list = false;
  } else {
    $('.list').removeClass('hide-list');
    $('.btn-hide-list').html('<i class="fas fa-times"></i>');
    list = true;
  }
};


// Detect scroll ends
let scrollEndDetector = () => {
  $('.list ul').on('scroll', function () {
    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight && wait === false) {
      wait = true;
      getData();
    }
  });
};

// get data from server
let getData = () => {
  if (totalPages === 0 || page <= totalPages) {
    // Show loader
    $('.list-loader').addClass('loader-active').html('<i class="fas fa-circle-notch fa-spin"></i><br><small>Cargando usuarios</small>');
    // init and get the first data
    Meteor.call('get.clientes.by.page', page, perPage, (error, response) => {
      if (!error) {
        response.data.forEach((element) => {
          users.push(element);
        });
        Session.set('users', users);
        page++;
        totalPages = response.totalPages;
        wait = false;
        // Hide loader
        $('.list-loader').removeClass('loader-active');
      }
    });
  } else {
    $('.list-loader').addClass('loader-active').html('<i class="fas fa-ban"></i><br><small>No hay mas usuarios</small>');
    setTimeout(() => {
      $('.list-loader').removeClass('loader-active').html('<i class="fas fa-circle-notch fa-spin"></i><br><small>Cargando  Usuarios</small>');
      wait = false;
    }, 2000);
  }
};
