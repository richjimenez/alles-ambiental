import './dashboard.html';

Template.adminDashboard.onCreated(function() {
  $('body').addClass('admin-body');
});


Template.adminDashboard.events({
  'click #logout' (e) {
    e.preventDefault();
    Meteor.logout(() => {
      location.reload();
    });
  },
  'click .btn-hide-list'(e) {
    $('.list').addClass('hide-list');
  },
  'click .btn-show-list'() {
    $('.list').removeClass('hide-list');
  },
});

Template.adminDashboard.onRendered(function() {
  if (document.body.clientWidth <= 414) {
    $('.list').addClass('hide-list');
  }
  window.addEventListener('resize', () => {
    const width = document.body.clientWidth;
    if (width <= 414) {
      $('.list').addClass('hide-list');
    } else {
      $('.list').removeClass('hide-list');
    }
  });
});
