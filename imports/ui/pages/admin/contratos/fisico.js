import './fisico.html';
import { Template } from 'meteor/templating';
import { Projects } from '../../../../api/projects/projects';

Template.contratoFisico.onCreated(function () {
  this.subscribe('get.project', FlowRouter.getParam('id'));
});
Template.contratoFisico.helpers({
  contrato() {
    return Projects.findOne().contrato;
  },
});
Template.contratoFisico.onRendered(function() {

});
