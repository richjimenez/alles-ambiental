import './add.html';
import { Template } from 'meteor/templating';
import { Projects } from '/imports/api/projects/projects';
import Contratos from '/imports/startup/client/contratos';
import { Session } from 'meteor/session';
import { Alert } from '/imports/startup/client/alerts';

import $ from 'jquery';
import moment from 'moment';

moment.locale('es');

Template.addContrato.onCreated(function () {

});

Template.addContrato.events({
  'submit #add-contrato'(event) {
    event.preventDefault();
    const form = event.target;
    const project = Projects.findOne();
    const folio = project.folio;
    const venta = form.venta.value;
    const cliente = `${project.client.nombre} ${project.client.apellidos}`;
    const direccion = project.client.direccion.completa;
    const sf = Session.get('sistemaFotovoltaico');
    const np = Session.get('numeroDePaneles');
    const tp = `${Session.get('potencia')} WP, risen energy`;
    const total = Session.get('grandTotal');
    const entrega = moment(form.entrega.value).format('D [de] MMMM [de] YYYY');
    const today = moment(new Date()).format('D [de] MMMM [de] YYYY');
    const kwhp = Session.get('energiaMensual');
    let pago;

    switch (project.pago) {
      case '50% y 50%':
        pago = 'de un anticipo del 50% cincuenta por ciento a la firma del presente contrato y un 50% cincuenta por '
          + 'ciento una vez terminada la instalación “EL EQUIPO” y hechas las pruebas de funcionamiento.';
        break;
      case '50%, 35% y 15%':
        pago = 'de un anticipo del 50% cincuenta por ciento a la firma del presente contrato y un 35% al termino etapa '
          + 'instalación paneles y estructura y el 15% restante al termino de instalación inversor y material eléctrico, '
          + 'realización de pruebas de sistema funcionando.';
        break;
      case '100% ecoCredito':
        pago = 'de 100% ecocredito.';
        break;
      default:
        Modal.hide('addContrato');
        throw Alert.error('Selecciona un metodo de pago');
    }
    let contrato;


    if (venta === 'SOLAR INTERCOM SA DE CV') {
      if (project.empresa) {
        const razonSocial = project.empresa.razon_social;
        contrato = Contratos.moral(folio, venta, cliente, razonSocial, sf, np, tp, total, pago,
          direccion, entrega, kwhp, today, project).replace(/(\r\n|\n|\r)/gm, '');
      } else {
        Alert.error('El cliente no tiene empresa');
      }
    } else {
      contrato = Contratos.fisico(folio, venta, cliente, sf, np, tp, total, pago,
        direccion, entrega, kwhp, today, project).replace(/(\r\n|\n|\r)/gm, '');
    }
    Meteor.call('insert.contrato', FlowRouter.getParam('id'), contrato, (error) => {
      if (!error) {
        Alert.success('Contrato generado');
        Modal.hide('addContrato');
      }
    });
  },
});

Template.addContrato.onRendered(function () {
  $('.select2').select2({ theme: 'bootstrap4', placeholder: 'Selecciona una opcion', minimumResultsForSearch: -1 });
  flatpickr('.entrega', {
    altInput: true,
    altFormat: 'F j, Y',
    dateFormat: 'Z',
  });
});
