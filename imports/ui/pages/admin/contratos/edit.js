import './edit.html';
import { Template } from 'meteor/templating';
import {Projects} from "../../../../api/projects/projects";
import moment from "./add";
import {Contratos} from "../../../../startup/client/contratos";
import {Alert} from "../../../../startup/client/alerts";

Template.editContrato.helpers({
  contrato() {
    return Projects.findOne().contrato;
  },
});

Template.editContrato.events({
  'submit #update-contrato'(event) {
    event.preventDefault();
    const form = event.target;

    const contrato = form.contrato.value;

    Meteor.call('insert.contrato', FlowRouter.getParam('id'), contrato, (error) => {
      if (!error) {
        Alert.success('Contrato actualizado');
        Modal.hide('editContrato');
      }
    });
  },
});


Template.editContrato.onRendered(function () {
  $('#contrato').trumbowyg();
});
