import './add-zone.html';
import swal from 'sweetalert2';

/* globals Template Meteor Modal $ */

Template.addZone.events({
  'keyup .number'(event) {
    const mes = event.target.parentNode.parentNode.id.split('-').pop();
    const mesSel = $(`#add-${mes}`);
    const dias = mesSel.find(`input[name="dias_${mes}"]`).val();
    const hrs = mesSel.find(`input[name="hrs_${mes}"]`).val();
    if (dias && hrs) mesSel.find('.result').text((hrs * dias).toFixed(2));
  },
  'submit #add-zone'(event) {
    event.preventDefault();
    const form = event.target;
    const data = {
      name: form.nombre.value,
      meses: {
        ene: {
          dias: Number(form.dias_ene.value),
          hrs: Number(form.hrs_ene.value),
        },
        feb: {
          dias: Number(form.dias_feb.value),
          hrs: Number(form.hrs_feb.value),
        },
        mar: {
          dias: Number(form.dias_mar.value),
          hrs: Number(form.hrs_mar.value),
        },
        abr: {
          dias: Number(form.dias_abr.value),
          hrs: Number(form.hrs_abr.value),
        },
        may: {
          dias: Number(form.dias_may.value),
          hrs: Number(form.hrs_may.value),
        },
        jun: {
          dias: Number(form.dias_jun.value),
          hrs: Number(form.hrs_jun.value),
        },
        jul: {
          dias: Number(form.dias_jul.value),
          hrs: Number(form.hrs_jul.value),
        },
        ago: {
          dias: Number(form.dias_ago.value),
          hrs: Number(form.hrs_ago.value),
        },
        sep: {
          dias: Number(form.dias_sep.value),
          hrs: Number(form.hrs_sep.value),
        },
        oct: {
          dias: Number(form.dias_oct.value),
          hrs: Number(form.hrs_oct.value),
        },
        nov: {
          dias: Number(form.dias_nov.value),
          hrs: Number(form.hrs_nov.value),
        },
        dic: {
          dias: Number(form.dias_dic.value),
          hrs: Number(form.hrs_dic.value),
        },
      },
    };
    Meteor.call('insert.zone', data, (err, res) => {
      if (!err) {
        swal({
          type: 'success',
          text: 'Zona Guardada',
          heightAuto: false,
        });
        Modal.hide('addZone');
      } else {
        swal({
          type: 'error',
          text: err.reason,
          heightAuto: false,
        });
      }
    });
  },
});

Template.addZone.onRendered(function() {
  const today = new Date();
  const meses = [
    'ene',
    'feb',
    'mar',
    'abr',
    'may',
    'jun',
    'jul',
    'ago',
    'sep',
    'oct',
    'nov',
    'dic',
  ];
  meses.forEach((mes, index) => {
    const dias = new Date(today.getFullYear(), index + 1, 0).getDate();
    $(`#add-${mes}`)
      .find(`input[name='dias_${mes}']`)
      .val(dias);
  });
});
