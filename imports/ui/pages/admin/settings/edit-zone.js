import './edit-zone.html';
import Zones from '/imports/api/zones/zones';
import swal from 'sweetalert2';

/* globals Template Session Meteor Modal $ */

Template.editZone.helpers({
  zona() {
    return Zones.findOne({ _id: Session.get('zoneId') });
  },
});
Template.editZone.events({
  'keyup .number'(event) {
    const mes = event.target.parentNode.parentNode.id.split('-').pop();
    const mesSel = $(`#edit-${mes}`);
    const dias = $(`#dias-${mes}`).val();
    const hrs = $(`#hrs-${mes}`).val();
    if (dias && hrs) {
      mesSel.find('.result').text((hrs * dias).toFixed(2));
    }
  },
  'submit #edit-zone'(event) {
    event.preventDefault();
    const form = event.target;
    const data = {
      name: form.nombre.value,
      meses: {
        ene: {
          dias: Number(form.dias_ene.value),
          hrs: Number(form.hrs_ene.value),
        },
        feb: {
          dias: Number(form.dias_feb.value),
          hrs: Number(form.hrs_feb.value),
        },
        mar: {
          dias: Number(form.dias_mar.value),
          hrs: Number(form.hrs_mar.value),
        },
        abr: {
          dias: Number(form.dias_abr.value),
          hrs: Number(form.hrs_abr.value),
        },
        may: {
          dias: Number(form.dias_may.value),
          hrs: Number(form.hrs_may.value),
        },
        jun: {
          dias: Number(form.dias_jun.value),
          hrs: Number(form.hrs_jun.value),
        },
        jul: {
          dias: Number(form.dias_jul.value),
          hrs: Number(form.hrs_jul.value),
        },
        ago: {
          dias: Number(form.dias_ago.value),
          hrs: Number(form.hrs_ago.value),
        },
        sep: {
          dias: Number(form.dias_sep.value),
          hrs: Number(form.hrs_sep.value),
        },
        oct: {
          dias: Number(form.dias_oct.value),
          hrs: Number(form.hrs_oct.value),
        },
        nov: {
          dias: Number(form.dias_nov.value),
          hrs: Number(form.hrs_nov.value),
        },
        dic: {
          dias: Number(form.dias_dic.value),
          hrs: Number(form.hrs_dic.value),
        },
      },
    };
    const id = Zones.findOne({ _id: Session.get('zoneId') })._id;
    Meteor.call('update.zone', id, data, (err) => {
      if (!err) {
        swal({
          type: 'success',
          text: 'Zona Actualizada',
          heightAuto: false,
        });
        Modal.hide('editZone');
      } else {
        swal({
          type: 'error',
          text: err.reason,
          heightAuto: false,
        });
      }
    });
  },
});
Template.editZone.onRendered(function() {
  const meses = [
    'ene',
    'feb',
    'mar',
    'abr',
    'may',
    'jun',
    'jul',
    'ago',
    'sep',
    'oct',
    'nov',
    'dic',
  ];
  meses.forEach((mes) => {
    const hrs = Number($(`#edit-${mes} #hrs-${mes}`).val());
    const dias = Number($(`#edit-${mes} #dias-${mes}`).val());
    $(`#edit-${mes} .result`).text((hrs * dias).toFixed(2));
  });
});
