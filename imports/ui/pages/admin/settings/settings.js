import './settings.html';
import './add-zone';
import './edit-zone';

import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Settings } from '/imports/api/settings/settings';
import { Alert } from '/imports/startup/client/alerts';
import Zones from '/imports/api/zones/zones';

/* globals Modal Session */

Template.adminSettings.onCreated(function() {
  this.subscribe('get.settings');
  this.subscribe('get.zones');
  this.autorun(() => {
    if (this.subscriptionsReady()) {
    }
  });
});
Template.adminSettings.helpers({
  tarifas() {
    return Settings.findOne({ _id: 'tarifas' });
  },
  zonas() {
    return Zones.find();
  },
  getResult(hrs, dias) {
    return (hrs * dias).toFixed(2);
  },
});
Template.adminSettings.events({
  'submit #update-settings'(event) {
    event.preventDefault();
    const form = event.target;
    const data = {
      dac: form.dac.value,
      pbdt: form.pbdt.value,
      gdmto: form.gdmto.value,
      gdmth: form.gdmth.value,
      watt: form.watt.value,
      cambio: form.cambio.value,
    };
    Meteor.call('update.tarifas', data, (error) => {
      if (!error) Alert.success('Las información se han actualizado');
    });
  },
  'click .add-zone'() {
    Modal.show('addZone');
  },
  'click .edit-zona'(event) {
    Modal.show('editZone');
    Session.set('zoneId', event.target.value);
  },
});
