import './zones.html';
import './zone';

/* globals $ Meteor Session Template FlowRouter */

// declare variables
let data; let page; let perPage; let totalPages; let wait;

// show & hide the list
let list = true;
const showHideList = () => {
  if (list === true) {
    $('.list').addClass('hide-list');
    $('.btn-hide-list').html('<i class="fas fa-list-ul"></i>');
    list = false;
  } else {
    $('.list').removeClass('hide-list');
    $('.btn-hide-list').html('<i class="fas fa-times"></i>');
    list = true;
  }
};

// get data from server
const getData = () => {
  if (totalPages === 0 || page <= totalPages) {
    // Show loader
    $('.list-loader').addClass('loader-active').html('<i class="fas fa-circle-notch fa-spin"></i><br><small>Cargando Zonas</small>');
    // init and get the first data
    Meteor.call('get.zones.by.page', page, perPage, (error, response) => {
      if (!error) {
        response.data.forEach((element) => {
          data.push(element);
        });
        Session.set('data', data);
        page += 1;
        totalPages = response.tp;
        wait = false;
        // Hide loader
        $('.list-loader').removeClass('loader-active');
      }
    });
  } else {
    $('.list-loader').addClass('loader-active').html('<i class="fas fa-ban"></i><br><small>No hay mas zonas</small>');
    setTimeout(() => {
      $('.list-loader').removeClass('loader-active').html('<i class="fas fa-circle-notch fa-spin"></i><br><small>Cargando Zonas</small>');
      wait = false;
    }, 2000);
  }
};

// Detect scroll ends
const scrollEndDetector = () => {
  $('.list ul').on('scroll', function () {
    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight && wait === false) {
      wait = true;
      getData();
    }
  });
};


Template.adminZones.onCreated(function () {
  // init variables values
  data = [];
  page = 1;
  perPage = Math.floor(window.innerHeight / 50);
  totalPages = 0;
  wait = false;
});

Template.adminZones.helpers({
  zones() {
    if (Session.get('data')) return Session.get('data');
    return [];
  },
  activeItem(id) {
    if (id === FlowRouter.getParam('id')) return 'active';
    return '';
  },
});

Template.adminZones.events({
  'click .btn-hide-list'() {
    showHideList();
  },
});

Template.adminZones.onRendered(function () {
  window.addEventListener('resize', () => {
    const width = document.body.clientWidth;
    if (width <= 414) {
      $('.list').addClass('hide-list');
      $('.btn-hide-list').html('<i class="fas fa-list-ul"></i>');
      list = false;
    } else {
      $('.list').removeClass('hide-list');
      $('.btn-hide-list').html('<i class="fas fa-times"></i>');
      list = true;
    }
  });
  scrollEndDetector();
  getData();
});

Template.adminZones.onDestroyed(function () {
  // clean the session variables
  Session.keys = {};
});
