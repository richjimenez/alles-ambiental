import './zone.html';
import Zones from '/imports/api/zones/zones';
import { Users } from '/imports/api/users/users';

/* globals $ Template FlowRouter */

Template.adminZone.onCreated(function () {
  this.subscribe('get.users');
  this.autorun(() => {
    // detect path change
    FlowRouter.watchPathChange();
    this.subscribe('get.zone', FlowRouter.getParam('id'));
  });
});

Template.adminZone.helpers({
  zone() {
    if (FlowRouter.getParam('id') === 'all') {
      return Zones.findOne();
    }
    return Zones.findOne({ _id: FlowRouter.getParam('id') });
  },
  getUsers(users) {
    if (users) {
      let result = '';
      Users.find({ _id: { $in: users } }).fetch().forEach((user) => {
        result += `<span class="badge badge-primary mr-2 py-2 px-2"><i class="fas fa-user"></i> ${user.profile.nombre} ${user.profile.apellidos}</span>`;
      });
      return result;
    }
    return '<span class="badge badge-primary py-2 px-2">N/A</span>';
  },
  getResult(hrs, dias) {
    return (hrs * dias).toFixed(2);
  },
});

Template.adminZone.events({
  'click .edit-btn'(event) {
    FlowRouter.go(`/admin/zones/edit/${$(event.target).data('id')}`);
  },
});
