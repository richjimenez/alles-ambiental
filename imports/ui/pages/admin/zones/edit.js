import './edit.html';
import Zones from '/imports/api/zones/zones';
import { Users } from '/imports/api/users/users';
import swal from 'sweetalert2';

/* globals $ Meteor Template FlowRouter */

Template.adminEditZone.onCreated(function () {
  this.autorun(() => {
    this.subscribe('get.zone', FlowRouter.getParam('id'));
    this.subscribe('get.users');
  });
});

Template.adminEditZone.helpers({
  zone() {
    return Zones.findOne();
  },
  users() {
    return Users.find();
  },
});

Template.adminEditZone.events({
  'keyup .number'(event) {
    const mes = event.target.parentNode.parentNode.id.split('-').pop();
    const mesSel = $(`#edit-${mes}`);
    const dias = $(`#dias-${mes}`).val();
    const hrs = $(`#hrs-${mes}`).val();
    if (dias && hrs) {
      mesSel.find('.result').text((hrs * dias).toFixed(2));
    }
  },
  'submit #edit-zone'(event) {
    event.preventDefault();
    const form = event.target;
    const data = {
      name: form.nombre.value,
      meses: {
        ene: {
          dias: Number(form.dias_ene.value),
          hrs: Number(form.hrs_ene.value),
        },
        feb: {
          dias: Number(form.dias_feb.value),
          hrs: Number(form.hrs_feb.value),
        },
        mar: {
          dias: Number(form.dias_mar.value),
          hrs: Number(form.hrs_mar.value),
        },
        abr: {
          dias: Number(form.dias_abr.value),
          hrs: Number(form.hrs_abr.value),
        },
        may: {
          dias: Number(form.dias_may.value),
          hrs: Number(form.hrs_may.value),
        },
        jun: {
          dias: Number(form.dias_jun.value),
          hrs: Number(form.hrs_jun.value),
        },
        jul: {
          dias: Number(form.dias_jul.value),
          hrs: Number(form.hrs_jul.value),
        },
        ago: {
          dias: Number(form.dias_ago.value),
          hrs: Number(form.hrs_ago.value),
        },
        sep: {
          dias: Number(form.dias_sep.value),
          hrs: Number(form.hrs_sep.value),
        },
        oct: {
          dias: Number(form.dias_oct.value),
          hrs: Number(form.hrs_oct.value),
        },
        nov: {
          dias: Number(form.dias_nov.value),
          hrs: Number(form.hrs_nov.value),
        },
        dic: {
          dias: Number(form.dias_dic.value),
          hrs: Number(form.hrs_dic.value),
        },
      },
    };
    const id = FlowRouter.getParam('id');
    Meteor.call('update.zone', id, data, (err) => {
      if (!err) {
        swal({
          type: 'success',
          text: 'Zona Actualizada',
          heightAuto: false,
        });
      } else {
        swal({
          type: 'error',
          text: err.reason,
          heightAuto: false,
        });
      }
    });
  },
  'submit #update-encargados'(event) {
    event.preventDefault();
    const id = FlowRouter.getParam('id');
    const users = $('#encargados').val();
    Meteor.call('update.encargados', id, users, (error) => {
      if (!error) {
        swal({
          type: 'success',
          text: 'Encargados Guardados',
          heightAuto: false,
        });
      } else {
        swal({
          type: 'error',
          text: error.reason,
          heightAuto: false,
        });
      }
    });
  },
  'submit #update-vendedores'(event) {
    event.preventDefault();
    const id = FlowRouter.getParam('id');
    const users = $('#vendedores').val();
    Meteor.call('update.vendedores', id, users, (error) => {
      if (!error) {
        swal({
          type: 'success',
          text: 'Vendedores Guardados',
          heightAuto: false,
        });
      } else {
        swal({
          type: 'error',
          text: error.reason,
          heightAuto: false,
        });
      }
    });
  },
});

Template.adminEditZone.onRendered(function() {
  this.autorun(() => {
    if (this.subscriptionsReady()) {
      // init months
      const meses = [
        'ene',
        'feb',
        'mar',
        'abr',
        'may',
        'jun',
        'jul',
        'ago',
        'sep',
        'oct',
        'nov',
        'dic',
      ];
      meses.forEach((mes) => {
        const hrs = Number($(`#edit-${mes} #hrs-${mes}`).val());
        const dias = Number($(`#edit-${mes} #dias-${mes}`).val());
        $(`#edit-${mes} .result`).text((hrs * dias).toFixed(2));
      });
    }
  });
  this.autorun(() => {
    // detect changes
    Zones.findOne();
    if (this.subscriptionsReady()) {
      const encargadosEnZona = Zones.findOne().encargados;
      const vendedoresEnZona = Zones.findOne().vendedores;
      const encargados = Users.find({ 'profile.rol': 'encargado' }).fetch().map(user => ({
        id: user._id,
        text: `${user.profile.nombre} ${user.profile.apellidos}`,
      }));
      const vendedores = Users.find({ 'profile.rol': 'vendedor' }).fetch().map(user => ({
        id: user._id,
        text: `${user.profile.nombre} ${user.profile.apellidos}`,
      }));
      // init selects
      $('#encargados').select2({ placeholder: 'Selecciona usuarios', data: encargados }).val(encargadosEnZona).change();
      $('#vendedores').select2({ placeholder: 'Selecciona usuarios', data: vendedores }).val(vendedoresEnZona).change();
    }
  });
});
