import './user.html';
import { Users } from '/imports/api/users/users';

/* globals Template FlowRouter */

Template.adminUser.onCreated(function () {
  this.autorun(() => {
    // detect path change
    FlowRouter.watchPathChange();
    this.subscribe('get.user', FlowRouter.getParam('id'));
  });
});

Template.adminUser.helpers({
  user() {
    if (FlowRouter.getParam('id') === 'all') {
      return Users.findOne();
    }
    return Users.findOne({ _id: FlowRouter.getParam('id') });
  },
});

Template.adminUser.events({
  'click .edit-btn'(e) {
    FlowRouter.go(`/admin/users/edit/${$(e.target).data('id')}`);
  },
});
