import './login.html';

import anime from 'animejs';

Template.login.onCreated(function () {
  this.autorun(() => {
    console.log(Meteor.loggingIn());
    console.log(Meteor.userId());
  });
});

Template.login.events({
  'submit #login-form'(e) {
    e.preventDefault();
    const username = e.target.username.value;
    const password = e.target.password.value;

    initLoginInfo();

    setTimeout(() => {
      Meteor.loginWithPassword(username, password, (error) => {
        if (!error) {
          $('#loading-info').html('<i class="far fa-check-circle"></i> Iniciado Sesion');
          setTimeout(() => {
            window.location.href = '/admin';
          }, 1000);
        } else {
          errorLoginInfo(error);
        }
      });
    }, 1500);
  },
});

Template.login.onRendered(function () {
  $(document).ready(() => {

  });
});

let initLoginInfo = () => {
  // remove all errors
  $('#user-input input').removeClass('is-invalid');
  $('#user-input .invalid-feedback').html();
  $('#password-input input').removeClass('is-invalid');
  $('#password-input .invalid-feedback').html();
  // show info
  $('.login-loading').fadeIn(1000);
  $('#loading-info').html('Autorizando credenciales <div class="dot">.</div><div class="dot">.</div><div class="dot">.</div>');
  const dotAnimate = anime({
    targets: '#loading-info .dot',
    translateX: 10,
    direction: 'alternate',
    loop: true,
    delay(el, i, l) {
      return i * 300;
    },
  });
};

let errorLoginInfo = (error) => {
  if (error.reason === 'Incorrect password') {
    $('#password-input input').addClass('is-invalid');
    $('#password-input .invalid-feedback').html('Contraseña incorrecta');
    $('#loading-info').html('<i class="fas fa-hand-paper"></i> Contraseña incorrecta');
  }
  if (error.reason === 'User not found') {
    $('#user-input input').addClass('is-invalid');
    $('#user-input .invalid-feedback').html('Usuario no encontrado');
    $('#loading-info').html('<i class="fas fa-hand-paper"></i> Usuario no encontrado');
  }
  setTimeout(() => {
    $('.login-loading').fadeOut(1000);
  }, 1500);
};
