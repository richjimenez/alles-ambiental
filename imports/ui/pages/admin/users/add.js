import "./add.html";
import {
  initAutoComplete,
  geolocate
} from "/imports/startup/client/autocomplete";
import swal from "sweetalert2";

Template.adminAddUser.onCreated(function() {});

Template.adminAddUser.helpers({});

Template.adminAddUser.events({
  "focus #address"() {
    geolocate();
  },
  "submit #add-user"(event) {
    event.preventDefault();
    $("input").prop("disabled", true);
    $("select").prop("disabled", true);
    $("button[type='submit']")
      .prop("disabled", true)
      .html('Guardando... <i class="fas fa-spin fa-spinner"></i>');
    const t = event.target;
    const user = {
      nombre: t.nombre.value,
      apellidos: t.apellidos.value,
      email: t.email.value,
      telefono: t.tel.value,
      mobile: t.mobile.value,
      rol: t.rol.value,
      direccion: {
        completa: t.address.value,
        numero: t.street_number.value,
        calle: t.street.value,
        ciudad: t.city.value,
        estado: t.state.value,
        codigo_postal: t.zip_code.value,
        pais: t.country.value
      }
    };
    Meteor.call("insert.user", user, (error, result) => {
      if (!error) {
        swal({
          type: "success",
          text: "Usuario Guardado",
          heightAuto: false
        }).then(() => {
          FlowRouter.go(`/admin/users/list/${result}`);
        });
      } else {
        console.warn(error);
      }
      sd;
    });
  }
});

Template.adminAddUser.onRendered(function() {
  setTimeout(() => {
    initAutoComplete();
  }, 500);
  $(".select2").select2({
    theme: "bootstrap4",
    placeholder: "Select one option",
    minimumResultsForSearch: -1
  });
});
