import './edit.html';

import { Users } from '/imports/api/users/users';
import { initAutoComplete, geolocate } from '/imports/startup/client/autocomplete';
import swal from 'sweetalert2';

Template.adminEditUser.onCreated(function () {
  this.autorun(() => {
    this.subscribe('get.user', FlowRouter.getParam('id'));
  });
});

Template.adminEditUser.helpers({
  user() {
    return Users.findOne({ _id: FlowRouter.getParam('id') });
  },
});

Template.adminEditUser.events({
  'focus #address'() {
    geolocate();
  },
  'submit #update-user-info'(e) {
    e.preventDefault();
    const id = FlowRouter.getParam('id');
    const t = e.target;
    const profile = {
      nombre: t.nombre.value,
      apellidos: t.apellidos.value,
      telefono: t.tel.value,
      mobile: t.mobile.value,
      rol: t.rol.value,
      direccion: {
        completa: t.address.value,
        numero: t.street_number.value,
        calle: t.street.value,
        ciudad: t.city.value,
        estado: t.state.value,
        codigo_postal: t.zip_code.value,
        pais: t.country.value,
      },
    };
    Meteor.call('update.user.info', id, profile, (error) => {
      if (!error) {
        swal({
          type: 'success',
          text: 'Usuario actualizado',
          heightAuto: false,
        }).then(() => {
          FlowRouter.go(`/admin/users/list/${id}`);
        });
      }
    });
  },
  'click .update-username'() {
    const id = FlowRouter.getParam('id');
    const username = $('#username').val();
    Meteor.call('update.username', id, username, (error) => {
      if (error) {
        console.log(error);
      } else {
        console.log('changed');
      }
    });
  },
});

Template.adminEditUser.onRendered(function() {
  setTimeout(() => {
    initAutoComplete();
    $('.select2').select2({ theme: 'bootstrap4', placeholder: 'Select one option', minimumResultsForSearch: -1 });
  }, 100);
});
