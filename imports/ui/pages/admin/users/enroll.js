import './enroll.html';

import swal from 'sweetalert2';

Template.adminEnroll.events({
  'submit #enroll'(event) {
    event.preventDefault();
    const password = event.target.password.value;
    const passwordConfirm = event.target.password_confirm.value;
    // validations
    if (password.length < 8 || passwordConfirm.length < 8) {
      swal({
        type: 'error',
        text: 'Contraseña demasiado corta',
        heightAuto: false,
      });
    } else if (password !== passwordConfirm) {
      swal({
        type: 'error',
        text: 'Contraseñas no coinciden',
        heightAuto: false,
      });
    } else {
      Accounts.resetPassword(FlowRouter.getParam('token'), password, (error) => {
        if (!error) {
          swal({
            type: 'success',
            text: 'Ya puedes iniciar sesión con tu contraseña',
            heightAuto: false,
          }).then(() => {
            FlowRouter.go('/login');
          });
        } else {
          swal('Something went wrong', error.reason, 'error');
        }
      });
    }
  },
});
