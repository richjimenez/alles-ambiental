import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import $ from 'jquery';
import swal from 'sweetalert2';

import './add.html';

Template.adminAddProduct.events({
  'change #tipo'(event) {
    if (event.target.value === 'Panel') {
      $('.panel').show();
      $('.inversor').hide();
      $('#i-name').prop('required', false);
      $('#watts, #volts, #p-name').prop('required', true);
    } else if (event.target.value === 'Inversor') {
      $('.panel').hide();
      $('.inversor').show();
      $('#watts, #volts, #p-name').prop('required', false);
      $('#i-name').prop('required', true);
    }
  },
  'submit #add-product'(event) {
    event.preventDefault();
    $('input').prop('disabled', true);
    $('select').prop('disabled', true);
    $("button[type='submit']").prop('disabled', true).html('Guardando... <i class="fas fa-spin fa-spinner"></i>');
    const form = event.target;
    let producto;
    if (form.tipo.value === 'Panel') {
      producto = {
        nombre: form.p_name.value,
        tipo: form.tipo.value,
        volts: Number(form.volts.value),
        watts: Number(form.watts.value),
      };
    } else if (form.tipo.value === 'Inversor') {
      producto = {
        nombre: form.i_name.value,
        tipo: form.tipo.value,
      };
    }

    Meteor.call('insert.product', producto, (error, result) => {
      if (!error) {
        swal({
          type: 'success',
          text: 'Producto Guardado',
          heightAuto: false,
        });
        FlowRouter.go(`/admin/catalog/list/${result}`);
      } else {
        console.error(error);
      }
    });
  },
});

Template.adminAddProduct.onRendered(function() {
  $('.select2').select2({ theme: 'bootstrap4', placeholder: 'Selecciona una opción', minimumResultsForSearch: -1 });
  $('#watts').mask('000');
});
