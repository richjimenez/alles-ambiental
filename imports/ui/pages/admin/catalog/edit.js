import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { FlowRouter } from 'meteor/kadira:flow-router';
import $ from 'jquery';
import swal from 'sweetalert2';
import './edit.html';

Template.adminEditProduct.onCreated(function() {
  Meteor.call('get.product', FlowRouter.getParam('id'), (error, result) => {
    if (!error) {
      Session.set('product', result);
    } else {
      console.warn(error);
    }
  });
});

Template.adminEditProduct.helpers({
  product() {
    if (Session.get('product')) return Session.get('product');
  },
});

Template.adminEditProduct.events({
  'submit #update-product'(event) {
    event.preventDefault();
    $('input').prop('disabled', true);
    $('select').prop('disabled', true);
    $("button[type='submit']").prop('disabled', true).html('Actualizando... <i class="fas fa-spin fa-spinner"></i>');
    const form = event.target;
    const p = Session.get('product');
    let producto;
    if (p.tipo === 'Panel') {
      producto = {
        id: FlowRouter.getParam('id'),
        tipo: p.tipo,
        nombre: form.nombre.value,
        watts: Number(form.watts.value),
        volts: Number(form.volts.value),
      };
    } else {
      producto = {
        id: FlowRouter.getParam('id'),
        tipo: p.tipo,
        nombre: form.nombre.value,
      };
    }

    Meteor.call('update.product', producto, (error) => {
      if (!error) {
        swal({
          type: 'success',
          text: 'Producto Actualizado',
          heightAuto: false,
        });
        FlowRouter.go('/admin/catalog/list/all');
      }
    });
  },
  'click #delete-btn'(event) {
    const id = event.target.value;
    swal({
      title: 'Estas seguro?',
      text: 'Una vez borrado este producto no se podra recuperar',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Borrar!',
      heightAuto: false,
    }).then((result) => {
      if (result.value) {
        Meteor.call('remove.product', id, (error) => {
          if (!error) {
            swal({
              title: 'Borrado', text: 'El producto a sido borrado', type: 'success', heightAuto: false,
            }).then(() => FlowRouter.go('/admin/catalog/list/all'));
          }
        });
      }
    });
  },
});

Template.adminEditProduct.onRendered(function() {
  Meteor.setTimeout(() => {
    $('.select2').select2({ theme: 'bootstrap4', placeholder: 'Selecciona una opcion', minimumResultsForSearch: -1 });
  }, 50);
});
