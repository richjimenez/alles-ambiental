import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import $ from 'jquery';
import swal from 'sweetalert2';
import './catalog.html';
import './product';

let data;
let page;
let perPage;
let totalPages;
let wait;
let list = true;

Template.adminCatalog.onCreated(function () {
  // init variables values
  data = [];
  page = 1;
  perPage = Math.floor(window.innerHeight / 50);
  totalPages = 0;
  wait = false;
});

Template.adminCatalog.helpers({
  products() {
    if (Session.get('data')) return Session.get('data');
  },
  activeRecord(id) {
    if (id === FlowRouter.getParam('id')) return 'active';
  },
});

Template.adminCatalog.events({
  'click .btn-hide-list'() {
    showHideList();
  },
});

Template.adminCatalog.onRendered(function () {
  window.addEventListener('resize', () => {
    const width = document.body.clientWidth;
    if (width <= 414) {
      $('.list').addClass('hide-list');
      $('.btn-hide-list').html('<i class="fas fa-list-ul"></i>');
      list = false;
    } else {
      $('.list').removeClass('hide-list');
      $('.btn-hide-list').html('<i class="fas fa-times"></i>');
      list = true;
    }
  });
  scrollEndDetector();
  getData();
});

// show & hide the list
const showHideList = () => {
  if (list === true) {
    $('.list').addClass('hide-list');
    $('.btn-hide-list').html('<i class="fas fa-list-ul"></i>');
    list = false;
  } else {
    $('.list').removeClass('hide-list');
    $('.btn-hide-list').html('<i class="fas fa-times"></i>');
    list = true;
  }
};
// Detect scroll ends
let scrollEndDetector = () => {
  $('.list ul').on('scroll', function () {
    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight && wait === false) {
      wait = true;
      getData();
    }
  });
};
// get data from server
let getData = () => {
  if (totalPages === 0 || page <= totalPages) {
    // Show loader
    $('.list-loader').addClass('loader-active').html('<i class="fas fa-circle-notch fa-spin"></i><br><small>Cargando Productos</small>');
    // init and get the first data
    Meteor.call('get.products.by.page', page, perPage, (error, response) => {
      if (!error) {
        response.data.forEach((element) => {
          data.push(element);
        });
        Session.set('data', data);
        page += 1;
        totalPages = response.totalPages;
        wait = false;
        // Hide loader
        $('.list-loader').removeClass('loader-active');
      }
    });
  } else {
    $('.list-loader').addClass('loader-active').html('<i class="fas fa-ban"></i><br><small>No hay mas productos</small>');
    setTimeout(() => {
      $('.list-loader').removeClass('loader-active').html('<i class="fas fa-circle-notch fa-spin"></i><br><small>Cargando Productos</small>');
      wait = false;
    }, 2000);
  }
};
