import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/kadira:flow-router';
import $ from 'jquery';
import swal from 'sweetalert2';
import './product.html';

Template.adminProduct.onRendered(function() {
  this.autorun(() => {
    // detect path change
    FlowRouter.watchPathChange();
    getData(FlowRouter.getParam('id'));
  });
});

Template.adminProduct.helpers({
  product() {
    if (Session.get('product')) return Session.get('product');
  },
});

Template.adminProduct.events({
  'click .edit-btn'(event) {
    FlowRouter.go(`/admin/catalog/edit/${event.currentTarget.value}`);
  },
});

let getData = (id) => {
  Meteor.call('get.product', id, (error, result) => {
    if (!error) {
      Session.set('product', result);
    } else {
      console.warn(error);
    }
  });
};