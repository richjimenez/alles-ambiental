import './index.html';

import Chart from 'chart.js';

/* globals Template Session */

Template.graficaAhorroAnual.onRendered(function() {
  Chart.pluginService.register({
    beforeRender (chart) {
      if (chart.config.options.showAllTooltips) {
        // create an array of tooltips
        // we can't use the chart tooltip because there is only one tooltip per chart
        chart.pluginTooltips = [];
        chart.config.data.datasets.forEach(function (dataset, i) {
          chart.getDatasetMeta(i).data.forEach(function (sector, j) {
            chart.pluginTooltips.push(new Chart.Tooltip({
              _chart: chart.chart,
              _chartInstance: chart,
              _data: chart.data,
              _options: chart.options,
              _active: [sector],
            }, chart));
          });
        });

        // turn off normal tooltips
        chart.options.tooltips.enabled = false;
      }
    },
    afterDraw (chart, easing) {
      if (chart.config.options.showAllTooltips) {
        // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
        if (!chart.allTooltipsOnce) {
          if (easing !== 1) return;
          chart.allTooltipsOnce = true;
        }

        // turn on tooltips
        chart.options.tooltips.enabled = true;
        Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
          tooltip.initialize();
          tooltip.update();
          // we don't actually need this since we are not animating tooltips
          tooltip.pivot();
          tooltip.transition(easing).draw();
        });
        chart.options.tooltips.enabled = false;
      }
    },
  });
  const config = {
    type: 'doughnut',
    data: {
      labels: [
        'SIN PANELES',
        'CON PANELES',
        'AHORRO ANUAL',
      ],
      datasets: [{
        data: [300, 50, 101],
        backgroundColor: ['#cf6527', '#23659a', '#88bc57'],
        borderWidth: 0,
      },
      ],
    },
    options: {
      cutoutPercentage: 30,
      responsive: true,
      maintainAspectRatio: false,
    },
  };


  const ctx = document.getElementById('ahorro-anual').getContext('2d');
  const myChart = new Chart(ctx, config);

  this.autorun(() => {
    const result = Number(Session.get('energiaMensual') * Session.get('tarifa').valor * 12).toFixed(2);
    myChart.data.datasets[0].data[0] = Number(Session.get('pagoActualCFE')).toFixed(2);
    myChart.data.datasets[0].data[1] = Session.get('pagoRealNoFormat').toFixed(2);
    myChart.data.datasets[0].data[2] = result;
    myChart.update();
  });
});
