import './header.html';


Template.adminHeader.events({
  'click .menu-toggle'() {
    console.log('hi');
    $('.admin-sidebar').toggleClass('hide-sidebar');
    if (menuStatus === true) {
      $('.menu-toggle').html('<i class="fas fa-bars"></i>');
      menuStatus = false;
    } else {
      $('.menu-toggle').html('<i class="fas fa-chevron-left"></i>');
      menuStatus = true;
    }
  },
  'click .users'(e) {
    e.preventDefault();
    FlowRouter.go('/admin/users');
  },
  'click #logout'() {
    Meteor.logout(() => {
      location.reload();
    });
  },
});
