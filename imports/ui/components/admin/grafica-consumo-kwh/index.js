import './index.html';

import Chart from 'chart.js';
import { Projects } from '/imports/api/projects/projects';
import Zones from '/imports/api/zones/zones';
import { Session } from 'meteor/session';
/* globals Template */

Template.graficaConsumoKWH.onRendered(function() {
  const ctx = document.getElementById('myChart').getContext('2d');
  ctx.height = 500;
  // eslint-disable-next-line no-new
  const chart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto',
        'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      datasets: [{
        label: 'Consumo KWH',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        backgroundColor: 'rgb(0,172,239)',
        borderWidth: 0,
      }],
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
          },
        }],
      },
    },
  });
  this.autorun(() => {
    const project = Projects.findOne();
    const kw = ((project.panel.watts / 1000) * Session.get('numeroDePaneles')).toFixed(2);
    const zone = Zones.findOne({ _id: project.zona });
    const data = [
      Number(((zone.meses.ene.hrs * zone.meses.ene.dias) * kw * 0.75).toFixed(2)),
      Number(((zone.meses.feb.hrs * zone.meses.feb.dias) * kw * 0.75).toFixed(2)),
      Number(((zone.meses.mar.hrs * zone.meses.mar.dias) * kw * 0.75).toFixed(2)),
      Number(((zone.meses.abr.hrs * zone.meses.abr.dias) * kw * 0.75).toFixed(2)),
      Number(((zone.meses.may.hrs * zone.meses.may.dias) * kw * 0.75).toFixed(2)),
      Number(((zone.meses.jun.hrs * zone.meses.jun.dias) * kw * 0.75).toFixed(2)),
      Number(((zone.meses.jul.hrs * zone.meses.jul.dias) * kw * 0.75).toFixed(2)),
      Number(((zone.meses.ago.hrs * zone.meses.ago.dias) * kw * 0.75).toFixed(2)),
      Number(((zone.meses.sep.hrs * zone.meses.sep.dias) * kw * 0.75).toFixed(2)),
      Number(((zone.meses.oct.hrs * zone.meses.oct.dias) * kw * 0.75).toFixed(2)),
      Number(((zone.meses.nov.hrs * zone.meses.nov.dias) * kw * 0.75).toFixed(2)),
      Number(((zone.meses.dic.hrs * zone.meses.dic.dias) * kw * 0.75).toFixed(2)),
    ];
    console.log(data);
    chart.data.datasets[0].data = data
    chart.update();
  });
});
