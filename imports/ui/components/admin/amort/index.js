import './index.html';
import moment from 'moment';
import Dinero from 'dinero.js';
import { Projects } from '../../../../api/projects/projects';

/* globals $ Template flatpickr Meteor Session */

function validateAmountDecimals(event) {
  const { value } = event.target;
  const rgx = /^[0-9]*\.?[0-9]*$/;
  $(event.target).val(...value.match(rgx));
}
function validateAmount(event) {
  const { value } = event.target;
  const rgx = /^[0-9]*$/;
  $(event.target).val(...value.match(rgx));
}
function calcAmortization(a) {
  // functions
  const monthlyPayment = (i, n, p) => p * i * ((1 + i) ** n) / (((1 + i) ** n) - 1);
  const formatAmount = v => Dinero({ amount: Number(v.toFixed(2).toString().replace('.', '')) }).toFormat('$0,0.00');

  // variables
  const total = Session.get('totalProjecto');
  const fondo = (total - a.aportacion) * (a.fondo / 100);
  const importe = total - a.aportacion - fondo;
  const tasaConIva = a.tasa * 1.16;
  const tasaDiaria = tasaConIva / 365;
  const interesMensual = tasaConIva / 12;
  const interesRealAnual = (tasaConIva - a.inflacion).toFixed(3);
  const interesAnual = (importe * interesRealAnual) / 100;
  const pagoMensual = monthlyPayment(interesMensual / 100, a.plazo, importe);
  // session variables
  Session.set('importeDespuesDeFondo', formatAmount(importe));
  Session.set('tasaConIva', tasaConIva);
  Session.set('tasaDiaria', tasaDiaria.toFixed(2));
  Session.set('interesMensual', interesMensual.toFixed(2));
  Session.set('fondo', formatAmount(fondo));
  Session.set('totalReembolso', formatAmount(pagoMensual * a.plazo));
  Session.set('interesRealAnual', interesRealAnual);
  Session.set('interesAnual', formatAmount(interesAnual));


  // console.log('Importe', importe);
  // console.log('Tasa con iva:', tasaConIva);
  // console.log('Tasa diaria:', tasaDiaria);
  // console.log('Interes mensual:', interesMensual);
  // console.log('Pago mensual', pagoMensual);

  // create data for table

  const result = [];
  let startDate = moment(a.fecha).add(a.dias, 'days');
  let initPayment = importe;
  let totalIntereses = 0;

  for (let i = 0; i < a.plazo; i += 1) {
    startDate = startDate.add(30, 'days');
    const intereses = initPayment * interesMensual / 100;
    const saldoFinal = initPayment - (pagoMensual - intereses);
    result.push({
      mes: i + 1,
      fecha: startDate.format('Do MMMM YYYY'),
      saldo: formatAmount(initPayment),
      amortization: formatAmount(pagoMensual - intereses),
      intereses: formatAmount(intereses),
      saldo_final: formatAmount(saldoFinal),
      pago: formatAmount(pagoMensual),
    });
    totalIntereses += intereses;
    initPayment = saldoFinal;
  }
  Session.set('totalIntereses', formatAmount(totalIntereses));
  Session.set('amortizationData', result);
}

Template.amortization.helpers({
  data() {
    return Projects.findOne().amortization;
  },
  importeDespuesDeFondo() {
    return Session.get('importeDespuesDeFondo');
  },
  totalProjecto(){
    return Session.get('totalProjecto')
  },
  tasaConIva() {
    return Session.get('tasaConIva');
  },
  tasaDiaria() {
    return Session.get('tasaDiaria');
  },
  interesMensual() {
    return Session.get('interesMensual');
  },
  fondo() {
    return Session.get('fondo');
  },
  totalReembolso() {
    return Session.get('totalReembolso');
  },
  totalIntereses() {
    return Session.get('totalIntereses');
  },
  interesRealAnual() {
    return Session.get('interesRealAnual');
  },
  interesAnual() {
    return Session.get('interesAnual');
  },
  formatAmount(amount) {
    const val = Number(amount.toFixed(2).toString().replace('.', ''));
    return Dinero({ amount: val }).toFormat('$0,0.00');
  },
  fechaVencimiento() {
    const a = Projects.findOne().amortization;
    return moment(a.fecha).add(a.dias, 'days').format('Do MMMM YYYY');
  },
  formatDate(date) {
    return moment(date).format('Do MMMM YYYY');
  },
  tableData() {
    return Session.get('amortizationData');
  },
});

Template.amortization.events({
  'keyup #tasa-interes' (event) {
    validateAmountDecimals(event);
  },
  'keyup #importe' (event) {
    validateAmountDecimals(event);
  },
  'keyup #aportacion' (event) {
    validateAmountDecimals(event);
  },
  'keyup #dias'(event) {
    validateAmount(event);
  },
  'keyup #plazo'(event) {
    validateAmount(event);
  },
  'submit #amortization-form'(event) {
    event.preventDefault();
    const id = Projects.findOne()._id;
    const form = event.target;
    const data = {
      tasa: Number(form.tasa.value),
      dias: Number(form.dias.value),
      plazo: Number(form.plazo.value),
      importe: Number(form.importe.value),
      aportacion: Number(form.aportacion.value),
      fondo: Number(form.fondo.value),
      fecha: new Date(form.fecha.value),
      inflacion: Number(form.inflacion.value),
    };
    Meteor.call('set.amortization', id, data, (err) => {
      if (!err) console.log('done');
    });
  },
});


Template.amortization.onRendered(function() {
  this.autorun(() => {
    const { amortization } = Projects.findOne();

    let fecha;
    if (amortization) {
      fecha = moment(amortization.fecha).toISOString();
      calcAmortization(amortization);
    } else fecha = moment().toISOString();

    flatpickr('#fecha', {
      altInput: true,
      altFormat: 'F j, Y',
      dateFormat: 'Z',
      defaultDate: fecha,
    });
  });
});
