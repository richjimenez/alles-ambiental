import './index.html';
import { Projects } from '../../../../api/projects/projects';
import { Alert } from '../../../../startup/client/alerts';

/* globals $ Template Meteor */

function showHideAmortization(val) {
  const fields = $('#amortization-fields');
  if (val === '100% ecoCredito') {
    fields.show();
    $('#amortization-fields .form-control').each(function() {
      if (!$(this).is('[readonly]')) {
        $(this).prop('required', true);
      }
    });
  } else {
    fields.hide();
    $('#amortization-fields .form-control').each(function() {
      if (!$(this).is('[readonly]')) {
        $(this).prop('required', false);
      }
    });
  }
}
function getData(form) {
  if (form.pago.value === '100% ecoCredito') {
    return {
      tasa: Number(form.tasa.value),
      dias: Number(form.dias.value),
      plazo: Number(form.plazo.value),
      aportacion: Number(form.aportacion.value),
      fondo: Number(form.fondo.value),
      fecha: new Date(form.fecha.value),
      inflacion: Number(form.inflacion.value),
    };
  }
  return {};
}

Template.formaDePago.onRendered(function() {
  showHideAmortization(Projects.findOne().pago);
});

Template.formaDePago.helpers({
  project() {
    return Projects.findOne();
  },
  data() {
    return Projects.findOne().amortization;
  },
});


Template.formaDePago.events({
  'change #pago'(event) {
    showHideAmortization(event.target.value);
  },
  'submit #forma-de-pago'(event) {
    event.preventDefault();
    const form = event.target;
    const id = Projects.findOne()._id;
    const paymentType = form.pago.value;
    const data = getData(form);

    Meteor.call('update.payment', id, paymentType, data, (err) => {
      if (!err) Alert.success('Información de pago actualizada');
    });
  },
});
