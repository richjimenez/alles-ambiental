import './sidebar.html';

Template.adminSidebar.onCreated(function() {

});

Template.adminSidebar.helpers({

});

Template.adminSidebar.events({
  'click .btn-hide-sidebar'() {
    $('.admin-sidebar').addClass('hide-sidebar');
    setTimeout(() => {
      $('.btn-show-sidebar').removeClass('hide');
    }, 300);
  },
  'click .btn-show-sidebar'() {
    $('.admin-sidebar').removeClass('hide-sidebar');
    $('.btn-show-sidebar').addClass('hide');
  },
});

Template.adminSidebar.onRendered(function() {
  if (document.body.clientWidth <= 768) {
    $('.admin-sidebar').addClass('hide-sidebar');
    $('.btn-show-sidebar').removeClass('hide');
    $('.menu-toggle').html('<i class="fas fa-bars"></i>');
  }
  window.addEventListener('resize', () => {
    const width = document.body.clientWidth;
    if (width <= 768) {
      $('.admin-sidebar').addClass('hide-sidebar');
      $('.btn-show-sidebar').removeClass('hide');
      $('.menu-toggle').html('<i class="fas fa-bars"></i>');
    } else {
      $('.admin-sidebar').removeClass('hide-sidebar');
      $('.btn-show-sidebar').addClass('hide');
      $('.menu-toggle').html('<i class="fas fa-chevron-left"></i>');
    }
  });
});
